import { STORE_VARS } from '../config';
import { TokenResponse } from '../types/fetchTypes';

export function get<T>(key: string): Promise<T> {
  return getStorageEngine()
    .get(key)
    .then((o) => (o[key] as unknown) as T);
}

export function getAll(keys: string[]): Promise<{ [key: string]: any }> {
  return getStorageEngine().get(keys);
}

export function set(key: string, value: any) {
  const obj: any = {};
  obj[key] = value;
  return getStorageEngine().set(obj);
}

export function getStorageEngine() {
  return browser.storage.local; // #10
}

export async function isTokenNeeded(): Promise<boolean> {
  try {
    const t = await get<TokenResponse>(STORE_VARS.EXTENSION_TOKEN);
    return typeof t === 'undefined';
  } catch (e) {
    console.error('isTokenNeeded()', e);
    return true;
  }
}
