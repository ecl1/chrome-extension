import * as storage from './storage';

describe('strage wrapper', () => {
  it('should get values from storage', async () => {
    const obj = { some: [{}, { json: 'Value' }], i: 6 };
    // @ts-ignore
    global.browser = {
      storage: {
        local: {
          get: () => Promise.resolve({ testLol: obj }),
        },
      },
    };
    expect(await storage.get('testLol')).toEqual(obj);
  });
  it('should getAll values from storage', async () => {
    const obj = { some: [{}, { json: 'Value' }], i: 6 };
    const obj2 = { some: [{}, { json: 'Value', and: 'string' }], i: 69 };
    // @ts-ignore
    global.browser = {
      storage: {
        local: {
          get: () => Promise.resolve({ testLol: obj, sthElse: obj2 }),
        },
      },
    };
    expect(await storage.getAll(['testLol', 'sthElse', 'thirdValue'])).toEqual({
      sthElse: obj2,
      testLol: obj,
    });
  });

  it('should set values to storage', async () => {
    const obj = { some: [{}, { json: 'Value' }], i: 6 };
    let val = null;
    // @ts-ignore
    global.browser = {
      storage: {
        local: {
          set: (o: any) => (val = o),
        },
      },
    };
    await storage.set('obj', obj);
    expect(val).toEqual({ obj });
  });

  it('should check if new token is needed', async () => {
    // @ts-ignore
    global.browser = {
      storage: {
        local: {
          get: (o: any) => Promise.resolve({ token: 'notundefined' }),
        },
      },
    };
    expect(await storage.isTokenNeeded()).toEqual(false);
    // @ts-ignore
    global.browser = {
      storage: {
        local: {
          get: (o: any) => Promise.resolve({}),
        },
      },
    };
    expect(await storage.isTokenNeeded()).toEqual(true);
  });
});
