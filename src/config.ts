/** Shared Runtime - config file */

export const ECL_HOST = `https://${
  ECLABRANCH === 'stable' ? 'www' : ECLABRANCH
}.ecl.gg`; // www.ecl.gg, beta.ecl.gg or staging.ecl.gg

export const ECL_ORGA_ID = 'edc12227-3b07-4c5e-9325-f223025628f3';

export const STORE_VARS = {
  EXTENSION_TOKEN: 'token', // T: TokenResponse
};
