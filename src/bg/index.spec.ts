import * as storage from '../shared/storage';
import {
  MsgErrorType,
  MsgRequest,
  MsgRequestType,
  MsgResponse,
} from '../types/msg';
import * as fetch from './fetch';
import * as bgIndex from './index';
import Mock = jest.Mock;
import { ECL_HOST } from '../config';

describe('Background Index', () => {
  it('should route correctly', async () => {
    const spy = spyOn(fetch, 'doWebRequest');

    //
    // Default
    //
    const msg1: MsgRequest<any> = { payload: null, type: -1 }; // invalid, should default
    const msg1Exp: MsgResponse<null> = {
      data: { type: MsgErrorType.TypeNotKnown, reason: null },
      error: true,
    };
    expect(await bgIndex.onMessage(msg1, undefined)).toEqual(msg1Exp);

    //
    // Fetch
    //
    const msg2: MsgRequest<any> = {
      payload: null,
      type: MsgRequestType.FetchRequest,
    };
    const msg2Exp = 'PlsRouteToFetch';
    spy.and.returnValue(Promise.resolve(msg2Exp));
    expect(await bgIndex.onMessage(msg2, undefined)).toEqual(msg2Exp);
  });

  it('onInit should addListener for onMessage and onInstall', () => {
    const messageStub = jest.fn();
    const installedStub = jest.fn();
    spyOn(storage, 'get').and.returnValue('test');
    // @ts-ignore
    global.browser = {
      browserAction: {
        setBadgeText: jest.fn(),
      },
      runtime: {
        onInstalled: {
          addListener: installedStub,
        },
        onMessage: {
          addListener: messageStub,
        },
      },
      storage: {
        local: {
          get: jest.fn,
          remove: jest.fn,
          set: jest.fn,
        },
      },
    };
    bgIndex.onInit();
    expect(messageStub).toHaveBeenCalled();
    expect(installedStub).toHaveBeenCalled();
  });

  it('onInstall should create a new tab on install', () => {
    const stub = stubTabsCreate();
    bgIndex.onInstall({
      id: 'anotherId',
      previousVersion: undefined,
      reason: 'install',
    });
    expect(stub.mock.calls[0][0]).toEqual({
      active: true,
      url: ECL_HOST + '/profile',
    });
  });

  it('onInstall should do nothing on update etc', () => {
    // No Event Details
    const stub = stubTabsCreate();
    bgIndex.onInstall(undefined);

    // Event Details not of reason installed
    expect(stub).not.toHaveBeenCalled();
    bgIndex.onInstall({
      id: 'stringLmao',
      previousVersion: undefined,
      reason: 'chrome_update',
    });
    expect(stub).not.toHaveBeenCalled();
  });

  describe('addBadgeToBrowserActionIfNotAuthed', () => {
    beforeEach(() => {
      // @ts-ignore
      global.browser = {
        browserAction: {
          setBadgeBackgroundColor: jest.fn(),
          setBadgeText: jest.fn(),
        },
        storage: {
          onChanged: {
            addListener: jest.fn(),
            removeListener: jest.fn(),
          },
        },
      };
    });
    it('should show badge and listen if token not in store', async () => {
      spyOn(storage, 'get').and.returnValue(Promise.resolve(undefined));
      await bgIndex.addBadgeToBrowserActionIfNotAuthed();
      expect(browser.browserAction.setBadgeText).toHaveBeenLastCalledWith({
        text: '!',
      });
      expect(
        browser.browserAction.setBadgeBackgroundColor,
      ).toHaveBeenLastCalledWith({ color: '#d49b24' });
      expect(browser.storage.onChanged.addListener).toHaveBeenCalled();

      browser.browserAction.setBadgeText = jest.fn(); // reset

      // call onEvent listener without changes to `token`
      // @ts-ignore
      browser.storage.onChanged.addListener.mock.calls[0][0]({});
      expect(browser.browserAction.setBadgeText).not.toHaveBeenCalled();
      expect(browser.storage.onChanged.removeListener).not.toHaveBeenCalled();

      // call onEvent listener with changes to `token`
      // @ts-ignore
      browser.storage.onChanged.addListener.mock.calls[0][0]({
        token: 'zsgduzas',
      });
      expect(browser.browserAction.setBadgeText).toHaveBeenLastCalledWith({
        text: '',
      });
      expect(browser.storage.onChanged.removeListener).toHaveBeenCalled();
    });
    it('should not show badge if token in store', async () => {
      spyOn(storage, 'get').and.returnValue(Promise.resolve('sth'));
      await bgIndex.addBadgeToBrowserActionIfNotAuthed();
      expect(browser.browserAction.setBadgeText).toHaveBeenLastCalledWith({
        text: '',
      });
      expect(
        browser.browserAction.setBadgeBackgroundColor,
      ).not.toHaveBeenCalled();
      expect(browser.storage.onChanged.addListener).not.toHaveBeenCalled();
    });
  });

  it('should clearStorage()', async () => {
    const KEYS_TO_REMOVE = ['PLAYERVOTE-MATCHROOM1PLAYER3'];

    Date.now = jest.fn().mockReturnValue(2629800000 + 10);
    const getStub = spyOn(storage, 'getAll').and.returnValue(
      Promise.resolve({
        'PLAYERVOTE-MATCHROOM1PLAYER1': 'u11',
        'PLAYERVOTE-MATCHROOM1PLAYER2': 'd10',
        'PLAYERVOTE-MATCHROOM1PLAYER3': 'u9',
        'token': { some: { object: 'lol' } },
      }),
    );
    const removeStub = jest.fn();
    spyOn(storage, 'getStorageEngine').and.returnValue({ remove: removeStub });

    await bgIndex.clearStorage();
    expect(removeStub).toHaveBeenLastCalledWith(KEYS_TO_REMOVE);
    expect(getStub).toHaveBeenCalled();
  });

  it('should check if we have to clear storage checkIfStorageFull()', async () => {
    global.setTimeout = jest.fn();
    const getstub = spyOn(storage, 'getAll').and.returnValue(
      Promise.resolve({}),
    );
    const removeStub = jest.fn();
    spyOn(storage, 'getStorageEngine').and.returnValue({ remove: removeStub });
    await bgIndex.checkIfStorageFull();
    expect(removeStub).not.toHaveBeenCalled();

    let longstr = '';
    for (let i = 0; i < 5_242_880; i++) {
      longstr += 'a';
    }
    getstub.and.returnValue(Promise.resolve({ key: longstr }));
    await bgIndex.checkIfStorageFull();
    expect(removeStub).toHaveBeenCalled();
  });
});

function stubTabsCreate(): Mock {
  const stub = jest.fn();
  // @ts-ignore
  global.browser = {
    tabs: {
      create: stub,
    },
  };
  return stub;
}
