import { OnInstalledDetails } from '../types/backgroundTypes';

import { ECL_HOST, STORE_VARS } from '../config';
import * as storage from '../shared/storage';
import {
  MsgErrorType,
  MsgRequest,
  MsgRequestType,
  MsgResponse,
} from '../types/msg';
import * as fetch from './fetch';

/** onInit is it's own function for testing purposes */
export function onInit() {
  browser.runtime.onInstalled.addListener(onInstall);
  browser.runtime.onMessage.addListener(onMessage);
  checkIfStorageFull();
  addBadgeToBrowserActionIfNotAuthed();
}
/** Triggered every time the extension gets installed and updated */
export function onInstall(e: OnInstalledDetails) {
  if (typeof e !== 'undefined') {
    if (e.reason === 'install') {
      // only care about initial installation
      browser.tabs.create({
        active: true,
        url: `${ECL_HOST}/profile`,
      });
    }
  }
}

/** If it errors it returns a MsgErrorResponse */
export function onMessage(
  message: MsgRequest<any>,
  sender: browser.runtime.MessageSender,
): Promise<MsgResponse<any>> {
  switch (message.type) {
    case MsgRequestType.FetchRequest:
      return fetch.doWebRequest(message);
    default:
      return Promise.resolve({
        data: { reason: null, type: MsgErrorType.TypeNotKnown },
        error: true,
      });
  }
}

export async function addBadgeToBrowserActionIfNotAuthed() {
  const onEvent = (changes: any) => {
    if (typeof changes[STORE_VARS.EXTENSION_TOKEN] !== 'undefined') {
      browser.browserAction.setBadgeText({ text: '' });
      browser.storage.onChanged.removeListener(onEvent);
    }
  };
  if (typeof (await storage.get(STORE_VARS.EXTENSION_TOKEN)) !== 'undefined') {
    browser.browserAction.setBadgeText({ text: '' });
    return;
  }
  browser.browserAction.setBadgeText({ text: '!' });
  browser.browserAction.setBadgeBackgroundColor({ color: '#d49b24' });
  browser.storage.onChanged.addListener(onEvent);
}

/** If I calculated everything correctly we exceed the storage capacity after about 5k matches with 9 upvotes each */
export async function checkIfStorageFull() {
  const quota = 5_242_880; // 5 MB
  const obj = await storage.getAll(undefined);
  const str = JSON.stringify(obj); // bit bigger than actual usage
  const bitLength = byteCount(str);
  const limit = quota * 0.9; // the limit we dont want to cross
  if (bitLength > limit) {
    await clearStorage();
  }
  setTimeout(checkIfStorageFull, 1000 * 60 * 10); // check every 10 minutes
}

function byteCount(s: string): number {
  return encodeURI(s).split(/%..|./).length - 1;
}

/** This function removes old votes in order to clean the storage needed */
export async function clearStorage() {
  const obj: { [key: string]: string } = await storage.getAll(undefined);

  const toDelete: string[] = [];

  for (const key in obj) {
    if (!obj.hasOwnProperty(key)) {
      continue;
    }
    if (key.startsWith('PLAYERVOTE-')) {
      const limit = Date.now() - 2629800000; // one month prior now()
      if (+obj[key].substr(1) < limit) {
        toDelete.push(key);
      }
    }
  }
  await storage.getStorageEngine().remove(toDelete);
}

if (typeof ECLAVERSION !== 'undefined') {
  // this variable is not available in unit tests
  onInit();
}
