import {
  FetchRequest,
  FetchResponse,
  FetchResponsePayload,
} from '../types/fetchTypes';
import { MsgErrorType } from '../types/msg';

/**
 * This is a wrapper around the fetch API.
 *
 * Why do we need this?
 * Well even though the WebExtensions API is a standard,
 * browsers behave differently in a lot of ways.
 * This is why we use the browser-polyfill and this is why we need to do this.
 * Appearently wen we send all requests from the bg script,
 * everything works fine.
 */
export async function doWebRequest(req: FetchRequest): Promise<FetchResponse> {
  const { url, options } = req.payload;
  // TODO: do some validation
  let respPayl: FetchResponsePayload = null;
  try {
    const resp = await fetch(url, options);
    respPayl = {
      headers: headersToObject(resp.headers),
      ok: resp.ok,
      status: resp.status,
      text: await resp.text(),
      url: resp.url,
    };
  } catch (err) {
    return {
      data: {
        reason: err.message,
        type: MsgErrorType.FetchError,
      },
      error: true,
    };
  }
  return {
    data: respPayl,
    error: false,
  };
}

export function headersToObject(headers: Headers): { [key: string]: string } {
  if (typeof headers === 'undefined') {
    return {};
  }
  const obj: { [key: string]: string } = {};
  // @ts-ignore
  const keys = Array.from(headers.keys()) as string[];

  for (const key of keys) {
    obj[key] = headers.get(key);
  }
  return obj;
}
