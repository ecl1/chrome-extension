import { FetchRequest, FetchResponsePayload } from '../types/fetchTypes';
import { MsgErrorResponse, MsgErrorType } from '../types/msg';
import { doWebRequest, headersToObject } from './fetch';

describe('Fetch Wrapper', () => {
  describe('doWebRequest()', () => {
    it('should handle valid requests', async () => {
      const expRespStr = JSON.stringify({
        some: 'values',
        this: {
          works: 1337,
        },
      });
      // @ts-ignore
      fetchMock.mockResponseOnce(expRespStr);
      const req1: FetchRequest = {
        payload: {
          options: {
            headers: {
              'Test-Key': 'Test-Value',
            },
            method: 'POST',
          },
          url: 'https://test.com/test/test2?test3=5&test6=lmao#test',
        },
        type: undefined, // already handled by the router
      };
      const resp = await doWebRequest(req1);
      expect(resp.error).toEqual(false);
      const data = resp.data as FetchResponsePayload;
      expect(data.ok).toEqual(true);
      expect(data.status).toEqual(200);
      expect(data.text).toEqual(expRespStr);
    });

    it('should handle failed requests', async () => {
      const errMsg = 'This Request faild error message';
      // @ts-ignore
      fetchMock.mockRejectOnce(new Error(errMsg));

      const req1: FetchRequest = {
        payload: {
          options: {
            headers: {
              'Test-Key': 'Test-Value',
            },
            method: 'POST',
          },
          url: 'https://test.com/test/test2?test3=5&test6=lmao#test',
        },
        type: undefined, // already handled by the router
      };

      const res = await doWebRequest(req1);
      expect(res.error).toEqual(true);
      expect(res.data as MsgErrorResponse<any>).toEqual({
        reason: errMsg,
        type: MsgErrorType.FetchError,
      });
    });
  });

  describe('headersToObject', () => {
    it('should return empty object when passed undefined', () => {
      expect(headersToObject(undefined)).toEqual({});
    });
    it('should return correct object', () => {
      const obj = {
        'Content-Type': 'text/xml',
        'OtherObject': 'sOk90-1',
      };
      const expObj = {
        'content-type': 'text/xml',
        'otherobject': 'sOk90-1',
      };
      const myHeaders = new Headers(obj);
      expect(headersToObject(myHeaders)).toEqual(expObj);
    });
  });
});
