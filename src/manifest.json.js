/* tslint:disable object-literal-sort-keys */

//
// NOTE: All js in here will be
// executed at build time
//

module.exports = (browser, package) => ({
    manifest_version: 2,
    name: "ECL 2.0",
    version: package.version,
    description: package.description,
    author: "European Community League",
    homepage_url: "https://ecl.gg/",
    icons: {
        "96": "assets/96x96.png",
    },
    permissions: [
        "*://*.ecl.gg/*",
        "storage",
    ],
    browser_action: {
        "default_title": "ECL 2.0",
        "default_popup": "popup.html",
    },
    content_scripts: [
        {
            matches: ["https://*.faceit.com/*"],
            js: [
                ...(browser !== "ff" ? ["browser-polyfill.min.js"] : []),
                "faceit.js",
            ],
        },
        {
            matches: ["https://*.ecl.gg/profile/*"],
            js: [
                ...(browser !== "ff" ? ["browser-polyfill.min.js"] : []),
                "afterinstall.js",
            ],
        },
    ],
    background: {
        scripts: [
            ...(browser !== "ff" ? ["browser-polyfill.min.js"] : []),
            "background.js",
        ],
    },
    web_accessible_resources: [
        "faceit_main_context.js",
        "assets/logo_240.webp",
        "assets/archivo-latin.woff2",
        "assets/tp.webp",
    ],
    content_security_policy: "default-src 'none'; " + // if not allowed below, block it
                                "script-src 'self'; " + // only allow scripts in local bundle
                                "child-src 'self'; " + // only allow iframes in bundle (needed for CHANGELOG)
                                "script-src-elem 'self'; " + // only allow script-tags src set to bundle
                                "style-src 'self'; " + // only allow styles in bundle
                                "font-src 'self'; " + // only allow fonts in bundle
                                "connect-src https://*.ecl.gg", // only allow XHR to ecl.gg over https
});
