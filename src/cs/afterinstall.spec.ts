import * as storage from '../shared/storage';
import * as afterInstall from './afterinstall';
import * as swal2bind from './swal2bind';

describe('After Install (Token handler)', () => {
  beforeEach(() => {
    installMode();
    addAddonTokenTRtoDOM();
  });

  it('should detect if isProfilePage()', () => {
    expect(
      afterInstall.isProfilePage(
        '/profile/30337e73-09b7-4f4f-b2fe-f26a806f74ca/',
      ),
    ).toEqual(true);
    expect(
      afterInstall.isProfilePage(
        '/profile/30337e73-09b7-4f4f-b2fe-f26a806f74ca',
      ),
    ).toEqual(true);
    expect(
      afterInstall.isProfilePage(
        '/profile/30337e73-09b7-4f4f-b2fe-f26a806f74ca/matches',
      ),
    ).toEqual(false);
  });
  it('should to DOM modifications (install)', () => {
    spyOn(storage, 'isTokenNeeded').and.returnValue(true);
    afterInstall.changeDOM('install');
    const button = document.getElementById(
      'TESTCONNECTBTN',
    ) as HTMLAnchorElement;
    const uselessButton = document.getElementById(
      'TESTREMOVED',
    ) as HTMLAnchorElement;
    expect(button.href).toEqual('http://localhost/#addon-token-tr');
    expect(button.innerHTML.trim()).toEqual('Authorize');
    expect(button.onclick).toEqual(afterInstall.onActivateClick);
    expect(uselessButton.innerHTML.trim()).toEqual('Not connected');
    expect(
      document.getElementsByTagName('html')[0].style.scrollBehavior,
    ).toEqual('smooth');
    expect(location.hash).toEqual('#addon-token-tr');
  });

  it('should to DOM modifications (success)', () => {
    afterInstall.changeDOM('success');
    const button = document.getElementById(
      'TESTCONNECTBTN',
    ) as HTMLAnchorElement;
    const uselessButton = document.getElementById(
      'TESTREMOVED',
    ) as HTMLAnchorElement;
    expect(button).toBe(null);
    expect(uselessButton.innerHTML.trim()).toEqual('Connected');
    let allElements = Array.from(document.querySelectorAll('span.active'));
    let myElements = [];
    for (const el of allElements) {
      if (el.innerHTML.trim().includes('Set Up')) {
        myElements.push(el);
      }
    }
    expect(myElements.length).toEqual(1);
  });

  describe('requestToken', () => {
    it('should requestToken correctly', async () => {
      const resp = {
        data: {
          headers: { 'content-type': 'application/json; utf-8' },
          text: JSON.stringify({}),
        },
        error: false,
      };
      // @ts-ignore
      global.browser = {
        runtime: {
          sendMessage: jest.fn().mockReturnValue(Promise.resolve(resp)),
        },
        storage: {
          local: {
            get: () => Promise.resolve(undefined),
            set: () => Promise.resolve(),
          },
        },
      };
      spyOn(storage, 'isTokenNeeded').and.returnValue(true);
      await afterInstall.requestToken();
      expect(true).toEqual(true); // no error until here? great!
    });

    it('should fail on failed request', async () => {
      const resp = {
        data: { reason: 'Unlucko ma dude' },
        error: true,
      };
      // @ts-ignore
      global.browser = {
        runtime: {
          sendMessage: jest.fn().mockReturnValue(Promise.resolve(resp)),
        },
      };
      try {
        spyOn(storage, 'isTokenNeeded').and.returnValue(true);
        await afterInstall.requestToken();
      } catch (e) {
        expect(e.message).toEqual(
          'Request failed: {"reason":"Unlucko ma dude"}',
        );
        return;
      }
      expect(true).toEqual(false); // no error until here? fuck!
    });

    it('should fail on html response', async () => {
      const resp = {
        data: {
          headers: { 'content-type': 'text/html' },
          text: '<invalid json because it\'s html>',
        },
        error: false,
      };
      // @ts-ignore
      global.browser = {
        runtime: {
          sendMessage: jest.fn().mockReturnValue(Promise.resolve(resp)),
        },
      };
      try {
        spyOn(storage, 'isTokenNeeded').and.returnValue(true);
        await afterInstall.requestToken();
      } catch (e) {
        expect(e.message).toEqual(
          'You are not logged in. Please reload the page and retry.',
        );
        return;
      }
      expect(true).toEqual(false); // no error until here? fuck!
    });
  });

  describe('afterInstall()', () => {
    it('should return when not on profile page', async () => {
      afterInstall._this.isProfilePage = () => false;
      afterInstall._this.changeDOM = () => ({});
      const storageSpy = spyOn(storage, 'isTokenNeeded').and.returnValue(false);
      await afterInstall._this.afterInstall();
      expect(storageSpy).not.toHaveBeenCalled();
    });
    it('should not return when on profile page', async () => {
      afterInstall._this.isProfilePage = () => true;
      afterInstall._this.changeDOM = () => ({});
      const storageSpy = spyOn(storage, 'isTokenNeeded').and.returnValue(false);
      await afterInstall._this.afterInstall();
      expect(storageSpy).toHaveBeenCalled();
    });
    it('should \'install\' when token needed', async () => {
      afterInstall._this.isProfilePage = () => true;
      const changeDOMSpy = spyOn(afterInstall._this, 'changeDOM').and.callFake(
        (arg) => expect(arg).toEqual('install'),
      );
      spyOn(storage, 'isTokenNeeded').and.returnValue(true);
      await afterInstall._this.afterInstall();
      expect(changeDOMSpy).toHaveBeenCalled();
    });
    it('should show \'success\' when token not needed', async () => {
      afterInstall._this.isProfilePage = () => true;
      const changeDOMSpy = spyOn(afterInstall._this, 'changeDOM').and.callFake(
        (arg) => expect(arg).toEqual('success'),
      );
      spyOn(storage, 'isTokenNeeded').and.returnValue(false);
      await afterInstall._this.afterInstall();
      expect(changeDOMSpy).toHaveBeenCalled();
    });
  });

  it('should run onActivateClick', (done) => {
    const spy = spyOn(swal2bind, 'errorModal').and.callFake(() => ({}));
    afterInstall._this.requestToken = () =>
      Promise.reject(new Error('Unluckomalucko'));
    afterInstall._this.onActivateClick();
    setTimeout(() => {
      expect(spy).toHaveBeenCalled();
      done();
    }, 50);
  });
});

function addAddonTokenTRtoDOM() {
  const str = `
  <table>
  <tbody>
  <tr style="display: none" id="addon-token-tr">
      <td style="display:flex" id="addon-token-td">
        <img  src="/assets/i/chrome_connect.png" style="border-radius: 50%;margin-right: 20px;width:50px;height:50px">
        <div style="display:grid;margin-bottom:0px">
          <span>ECL Extension</span>
          <span style="font-size:14px;">
            <a id="TESTREMOVED" href="google.com" class="link_clickable" style="color:#bf8b21">Text #1</a>
          </span>
      </div>
      </td>
      <td><span><a id="TESTCONNECTBTN" href="" class="link_clickable" style="color:#bf8b21">Connect</a></span></td>
  </tr>
  </tbody>
  </table>`;
  document.body.innerHTML = str;
}

function installMode() {
  spyOn(storage, 'get').and.returnValue(Promise.resolve(undefined));
}
