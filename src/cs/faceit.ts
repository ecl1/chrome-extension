import { ECL_HOST, STORE_VARS } from '../config';
import { isTokenNeeded } from '../shared/storage';
import * as storage from '../shared/storage';
import { ShouldHidePlayersRequest } from '../types/faceitMainTypes';
import {
  FetchRequest,
  FetchResponse,
  TokenResponse,
} from '../types/fetchTypes';
import { MsgRequestType } from '../types/msg';
import { errorModal, showConnectionModal } from './swal2bind';

/** This Script will be injected into Faceit */

export async function initFaceitContentScript() {
  // @ts-ignore
  if (window.ecl_2_0_init) {
    // only execute once per tab
    return;
  }
  // @ts-ignore
  window.ecl_2_0_init = true;
  if (location.host === 'cdn.faceit.com') {
    return; // we dont want to inject into certain pages
  }
  if (await isTokenNeeded()) {
    showConnectionModal();
  } else {
    injectMainScript();
    document.addEventListener('ECL-2-0-XHR-REQUEST', (e: CustomEvent) => {
      onXHRRequest(e).catch((err: Error) => errorModal(err.message));
    });
    document.addEventListener('ECL-2-0-ERROR-MESSAGE', mainContextErrorHandler);
    document.addEventListener('ECL-2-0-VOTED-FOR-PLAYER', onVotedForPlayer);
    document.addEventListener(
      'ECL-2-0-SHOULD-HIDE-PLAYERS-REQ',
      (e: CustomEvent<ShouldHidePlayersRequest>) => {
        onShouldHidePlayersEvent(e);
      },
    );
  }
}

export function injectMainScript() {
  // Makes browser.runtime.getURL available for main script
  let urlScriptEl = document.createElement('script');
  urlScriptEl.setAttribute('type', 'application/javascript');
  urlScriptEl.innerHTML = `window.ECL_2_0_URL = "${browser.runtime.getURL(
    '/',
  )}"`;
  document.body.appendChild(urlScriptEl);

  // injects main script
  let mainScriptEl = document.createElement('script');
  mainScriptEl.setAttribute('type', 'application/javascript');
  mainScriptEl.src = browser.runtime.getURL('/faceit_main_context.js');
  document.body.appendChild(mainScriptEl);
}

/**
 * Note on Security:
 *
 * Everything that has write access to
 * FaceIt's DOM can dispatch such an event. This includes
 * FaceIt itself, their dependencies as well as other
 * Web Extensions. Therefore this should (backend-wise) not
 * have more permissions than it needs to.
 *
 * This is why the token is added here (requestForwarder)
 * and not given to the main-context script.
 * THE TOKEN SHOULD NEVER LEAK INTO THE
 * MAIN CONTEXT AS IT DOES NOT DIE.
 *
 */
export async function onXHRRequest(e: CustomEvent) {
  const d = e.detail as string;
  const dObj = JSON.parse(d) as {
    id: string;
    url: string;
    options: RequestInit;
  };
  const resp = await requestForwarder(dObj.url, dObj.options);
  const respEvent = new CustomEvent(`ECL-2-0-XHR-${dObj.id}`, {
    detail: JSON.stringify(resp),
  });
  document.dispatchEvent(respEvent);
}

export async function requestForwarder(
  url: string,
  options: RequestInit = {},
): Promise<FetchResponse> {
  // Make sure headers field exists
  if (!options.headers) {
    options.headers = {};
  }

  if (new RegExp(`^${ECL_HOST}`, 'i').test(url)) {
    try {
      const token = await storage.get<TokenResponse>(
        STORE_VARS.EXTENSION_TOKEN,
      );
      (options.headers as Record<string, string>).Authorization = token.token;
    } catch (err) {
      console.error('storage.get(token) failed', err);
      throw new Error('The extension is not connected to ECL yet');
    }
  } else {
    throw new Error('Host is not an ECL domain');
  }

  return browser.runtime.sendMessage<FetchRequest>({
    payload: {
      options,
      url,
    },
    type: MsgRequestType.FetchRequest,
  });
}

/**
 * Invoked when the main context script emmits an
 * "ECL-2-0-ERROR-MESSAGE" event
 */
export function mainContextErrorHandler(
  e: CustomEvent<{ msg: string; type: any }>,
) {
  const str = `${e.detail.msg}${
    typeof e.detail.type !== 'undefined' ? ` (Code: ${e.detail.type})` : ''
  }`;
  errorModal(str);
}

export async function onShouldHidePlayersEvent(
  event: CustomEvent<ShouldHidePlayersRequest>,
): Promise<any> {
  const { random, pl } = event.detail; // pl: <matchroom><playeruuid>
  const keys = pl.map((s: string) => 'PLAYERVOTE-' + s);
  const resp = await storage.getAll(keys);
  const objToReturn: { [str: string]: string } = {};
  for (const key of keys) {
    objToReturn[key.replace('PLAYERVOTE-', '')] =
      typeof resp[key] === 'undefined'
        ? ''
        : resp[key][0] === 'u'
        ? 'upvote'
        : 'downvote';
  }
  const respEvent = new CustomEvent(`ECL-2-0-SHOULD-HIDE-PLAYERS-${random}`, {
    detail: objToReturn,
  });
  document.dispatchEvent(respEvent);
}

export function onVotedForPlayer(event: CustomEvent<string>) {
  const key = 'PLAYERVOTE-' + event.detail.substr(1);
  storage.set(key, event.detail[0] + Date.now());
}

if (typeof ECLAVERSION !== 'undefined') {
  initFaceitContentScript();
}
