import Swal from 'sweetalert2';
import { ECL_HOST } from '../config';
import { errorModal, showConnectionModal } from './swal2bind';

describe('swal2bind', () => {
  it('should show error message', async () => {
    let arg;
    const spy = spyOn(Swal, 'fire').and.callFake((obj) => (arg = obj));
    errorModal('Nice Error Message');
    expect(spy).toHaveBeenCalled();
    expect(arg).toEqual({
      background: 'rgba(20,20,20,1)',
      icon: 'error',
      showConfirmButton: true,
      text: 'Nice Error Message',
      title: 'An error occured',
    });
  });
  it('should showConnectionModal', async () => {
    // @ts-ignore
    window.open = jest.fn().mockImplementation((arg1, arg2) => {
      expect(arg1).toEqual(`${ECL_HOST}/profile`);
      expect(arg2).toEqual('_self');
    });
    let arg;
    const spy = spyOn(Swal, 'fire').and.callFake((obj) => {
      arg = obj;
      return Promise.resolve({ value: true });
    });
    await showConnectionModal();
    expect(spy).toHaveBeenCalled();
    expect(arg).toEqual({
      background: 'rgba(20,20,20,1)',
      cancelButtonText: 'Later',
      confirmButtonText: 'Redirect me',
      icon: 'warning',
      showCancelButton: true,
      showConfirmButton: true,
      text: 'Connect the ECL 2.0 addon to your ECL account first',
      title: 'Before you continue',
    });
    expect(window.open).toHaveBeenCalled();
  });
});
