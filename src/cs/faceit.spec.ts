import { ECL_HOST } from '../config';
import * as storage from '../shared/storage';
import { ShouldHidePlayersRequest } from '../types/faceitMainTypes';
import { MsgRequestType } from '../types/msg';
import * as faceit from './faceit';
import * as swal2bind from './swal2bind';

describe('faceit', () => {
  it('should injectMainScript', () => {
    document.body.innerHTML = '';
    // @ts-ignore
    global.browser = {
      runtime: {
        getURL: () => 'kasmdasdjaknd:///skldmandjna/sadua.js',
      },
    };
    faceit.injectMainScript();
    const els = document.querySelectorAll('script');
    const el0 = els[0];
    const el1 = els[1];
    expect(el0.innerHTML).toEqual(
      `window.ECL_2_0_URL = "kasmdasdjaknd:///skldmandjna/sadua.js"`,
    );
    expect(el1.src).toEqual('kasmdasdjaknd:///skldmandjna/sadua.js');
    expect(el0.type).toEqual('application/javascript');
    expect(el1.type).toEqual('application/javascript');
    document.body.innerHTML = '';
  });

  it('should initFaceitContentScript once', async () => {
    document.body.innerHTML = '';
    // @ts-ignore
    global.browser = {
      runtime: {
        getURL: () => 'kasmdasdjaknd:///skldmandjna/sadua.js',
      },
    };
    const stub = spyOn(storage, 'isTokenNeeded').and.returnValue(
      Promise.resolve(false),
    );
    const stubSwal = spyOn(swal2bind, 'showConnectionModal').and.returnValue(
      Promise.resolve(undefined),
    );
    // @ts-ignore
    window.ecl_2_0_init = undefined;

    await faceit.initFaceitContentScript();
    // @ts-ignore
    expect(window.ecl_2_0_init).toEqual(true);
    expect(stub).toHaveBeenCalled();
    const el = document.querySelectorAll('script')[1];
    expect(el.src).toEqual('kasmdasdjaknd:///skldmandjna/sadua.js');
    expect(stubSwal).not.toHaveBeenCalled();

    stub.and.returnValue(Promise.resolve(true));
    // call again
    await faceit.initFaceitContentScript();
    expect(stubSwal).not.toHaveBeenCalled();

    // reset

    // @ts-ignore
    window.ecl_2_0_init = undefined;
    await faceit.initFaceitContentScript();
    expect(stubSwal).toHaveBeenCalled();
  });

  describe('requestForwarder() and onXHRRequest()', () => {
    let sendMessageMock: jest.Mock;
    let storageSpy: jasmine.Spy;
    beforeEach(() => {
      sendMessageMock = jest.fn();
      // @ts-ignore
      global.browser = {
        runtime: {
          sendMessage: sendMessageMock,
        },
      };
      storageSpy = spyOn(storage, 'get').and.returnValue(
        Promise.resolve({ userId: 123, token: 'SomeToken' }),
      );
    });
    it('should not foreward "other" requests', async () => {
      const opts = {
        body: 'bodyValue',
        headers: { Authorization: 'Value' },
        method: 'POST',
      };
      try {
        await faceit.requestForwarder('test', opts);
      } catch (e) {
        expect(e.message).toEqual('Host is not an ECL domain');
      }
      expect(sendMessageMock).not.toHaveBeenCalled();
    });
    it('should append token header if sent to ECL_HOST', async () => {
      const url = `${ECL_HOST}/test/lamo?q=5&Key=Value#hash`;
      await faceit.requestForwarder(url);
      expect(sendMessageMock).toHaveBeenLastCalledWith({
        payload: {
          options: {
            headers: {
              Authorization: 'SomeToken',
            },
          },
          url,
        },
        type: MsgRequestType.FetchRequest,
      });
    });

    it('should override token header if sent to ECL_HOST', async () => {
      const url = `${ECL_HOST}/test/lamo?q=5&Key=Value#hash`;
      await faceit.requestForwarder(url, {
        body: 'someBody',
        headers: { Authorization: 'Bearer OtherValues' },
      });
      expect(sendMessageMock).toHaveBeenLastCalledWith({
        payload: {
          options: {
            body: 'someBody',

            headers: {
              Authorization: 'SomeToken',
            },
          },
          url,
        },
        type: MsgRequestType.FetchRequest,
      });
    });

    it('should error if token not set (undefined.token access)', async () => {
      console.error = jest.fn(); // dont spam errors when testing
      storageSpy.and.returnValue(Promise.resolve(undefined));
      const url = `${ECL_HOST}/test/lamo?q=5&Key=Value#hash`;
      try {
        await faceit.requestForwarder(url);
        expect(false).toEqual(true); // dont execute this, b/c it should fail before
      } catch (err) {
        expect(err.message).toEqual(
          'The extension is not connected to ECL yet',
        );
      }
    });

    it('should error if token access failed', async () => {
      storageSpy.and.returnValue(Promise.reject());
      const url = `${ECL_HOST}/test/lamo?q=5&Key=Value#hash`;
      try {
        await faceit.requestForwarder(url);
        expect(false).toEqual(true); // dont execute this, b/c it should fail before
      } catch (err) {
        expect(err.message).toEqual(
          'The extension is not connected to ECL yet',
        );
      }
    });

    it('onXHRRequest should call requestForwarder and respond', async (next) => {
      sendMessageMock.mockReturnValue(
        Promise.resolve({ theSame: 'objectAsYouThink' }),
      );
      const event = new CustomEvent('does-not-matter', {
        detail: JSON.stringify({
          id: 'idString',
          options: { body: JSON.stringify({ some: { json: ['value'] } }) },
          url: ECL_HOST + 'someUrlString',
        }),
      });
      document.addEventListener('ECL-2-0-XHR-idString', (e: CustomEvent) => {
        expect(e.detail).toEqual(
          JSON.stringify({ theSame: 'objectAsYouThink' }),
        );
        next();
      });
      await faceit.onXHRRequest(event);
    });
  });

  it('should handle onShouldHidePlayersEvent', async (next) => {
    spyOn(storage, 'getAll').and.returnValue(
      Promise.resolve({
        'PLAYERVOTE-MATCHROOMPLAYER1': 'd1234567',
        'PLAYERVOTE-MATCHROOMPLAYER3': 'u78990',
      }),
    );
    const event = {
      detail: {
        pl: ['MATCHROOMPLAYER1', 'MATCHROOMPLAYER2', 'MATCHROOMPLAYER3'],
        random: 1,
      },
    } as CustomEvent<ShouldHidePlayersRequest>;

    const onResp = (ev: CustomEvent<{ [str: string]: boolean }>) => {
      document.removeEventListener('ECL-2-0-SHOULD-HIDE-PLAYERS-1', onResp);
      expect(ev.detail).toEqual({
        MATCHROOMPLAYER1: 'downvote',
        MATCHROOMPLAYER2: '',
        MATCHROOMPLAYER3: 'upvote',
      });
      next();
    };
    document.addEventListener('ECL-2-0-SHOULD-HIDE-PLAYERS-1', onResp);

    await faceit.onShouldHidePlayersEvent(event);
  });

  it('should handle onVotedForPlayer', (next) => {
    Date.now = jest.fn().mockReturnValue(694201337);
    spyOn(storage, 'set').and.callFake((key: string, value: number) => {
      expect(key).toEqual('PLAYERVOTE-MATCHROOMPLAYER1');
      expect(value).toEqual('u694201337');
      next();
    });
    faceit.onVotedForPlayer({ detail: 'uMATCHROOMPLAYER1' } as CustomEvent<
      string
    >);
  });
});
