import { ECL_HOST, STORE_VARS } from '../config';
import * as storage from '../shared/storage';
import {
  FetchRequest,
  FetchResponse,
  FetchResponsePayload,
  TokenResponse,
} from '../types/fetchTypes';
import { MsgRequestType } from '../types/msg';
import * as swal2bind from './swal2bind';

/** This script will be injected into ecl.gg */

const TOKEN_REQ: FetchRequest = {
  payload: {
    options: {
      credentials: 'include',
      method: 'GET',
    },
    url: `${ECL_HOST}/api/token`,
  },
  type: MsgRequestType.FetchRequest,
};

const HIGHLIGHT_CSS = `
.addon-highlight td {
  animation-delay: 1s;
  animation-name: addon-highlight-kf;
  animation-duration: 3s;
  border-radius: 10px;
}
@keyframes addon-highlight-kf {
  from {
      -webkit-box-shadow: 0px 0px 20px 11px white;
      -moz-box-shadow: 0px 0px 20px 11px white;
      box-shadow: 0px 0px 20px 11px white;
  }
  to {
      -webkit-box-shadow: 0px 0px 0 0px rbga(255, 255, 255, 0);
      -moz-box-shadow: 0px 0px 0px 0 rbga(255, 255, 255, 0);
      box-shadow: 0px 0px 0px 0 rbga(255, 255, 255, 0);
  }
}`.replace(/[\n ]/, '');

export async function afterInstall() {
  if (!_this.isProfilePage(location.pathname)) {
    return;
  }
  if (await storage.isTokenNeeded()) {
    _this.changeDOM('install');
  } else {
    _this.changeDOM('success');
  }
}

export function isProfilePage(path: string): boolean {
  const regex = /^\/profile\/[0-9a-z-]+\/?$/i;
  return regex.test(path);
}

export function changeDOM(mode: 'install' | 'success') {
  const TR_ID = 'addon-token-tr';
  const trEl = document.getElementById(TR_ID) as HTMLTableRowElement;
  if (trEl === null) {
    return;
  }
  const anchors = trEl.querySelectorAll('a.link_clickable');
  const subHeading = anchors[0] as HTMLAnchorElement;
  const anchor = anchors[1] as HTMLAnchorElement;
  const htmlElement = document.getElementsByTagName('html')[0];

  trEl.style.display = 'table-row';
  subHeading.href = '#' + TR_ID;

  if (mode === 'install') {
    subHeading.innerHTML = 'Not connected';
    anchor.href = '#' + TR_ID; // no-op
    anchor.innerHTML = 'Authorize';
    anchor.onclick = onActivateClick; // Add custom logic
    htmlElement.style.scrollBehavior = 'smooth'; // make scrolling smooth
    const styleTag = document.createElement('style');
    styleTag.innerHTML = HIGHLIGHT_CSS;
    document.head.appendChild(styleTag);
    location.hash = TR_ID; // scroll to button
    trEl.classList.add('addon-highlight');
  } else {
    subHeading.innerHTML = 'Connected';
    const successMessageElement = document.createElement('span');
    successMessageElement.className = 'active';
    successMessageElement.innerHTML = 'Set Up';
    anchor.replaceWith(successMessageElement);
  }
}

export function onActivateClick() {
  _this.requestToken().catch((err) => swal2bind.errorModal(err.message));
}

export async function storeToken(tokenResponse: TokenResponse): Promise<any> {
  await storage.set(STORE_VARS.EXTENSION_TOKEN, tokenResponse);
  changeDOM('success');
}

export async function requestToken() {
  if (!(await storage.isTokenNeeded())) {
    throw new Error('Already authorized');
  }
  const resp: FetchResponse = await browser.runtime.sendMessage(
    undefined,
    TOKEN_REQ,
  );
  if (resp.error) {
    throw new Error('Request failed: ' + JSON.stringify(resp.data));
  }
  const data = resp.data as FetchResponsePayload;

  if (!data.headers['content-type'].includes('application/json')) {
    // we landed on faceit's auth page, because we are not logged in
    throw new Error('You are not logged in. Please reload the page and retry.');
  }

  const tokenResponse = JSON.parse(data.text) as TokenResponse;
  await storeToken(tokenResponse);
}

export const _this = {
  afterInstall,
  changeDOM,
  isProfilePage,
  onActivateClick,
  requestToken,
  storeToken,
};

if (typeof ECLAVERSION !== 'undefined') {
  // not available in tests
  afterInstall();
}
