import Swal from 'sweetalert2';
import { ECL_HOST } from '../config';

export function showConnectionModal(): Promise<any> {
  return Swal.fire({
    background: 'rgba(20,20,20,1)',
    cancelButtonText: 'Later',
    confirmButtonText: 'Redirect me',
    icon: 'warning',
    showCancelButton: true,
    showConfirmButton: true,
    text: 'Connect the ECL 2.0 addon to your ECL account first',
    title: 'Before you continue',
  })
    .then((res: { value?: boolean }) => {
      if (res.value) {
        window.stop(); // stop faceit from loading, else it looks janky
        window.open(`${ECL_HOST}/profile`, '_self');
      }
    })
    .catch();
}

export function errorModal(text: string) {
  Swal.fire({
    background: 'rgba(20,20,20,1)',
    icon: 'error',
    showConfirmButton: true,
    text,
    title: 'An error occured',
  });
}
