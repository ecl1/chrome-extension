import * as mainCtxt from './faceit_main_context';
import { handleError } from './faceit_main_context';
import * as karma from './karma';
import * as karmaButtons from './karmaButtons';
import * as matchRoom from './matchRoom';
import * as report from './report';
import * as shared from './shared';

describe('Faceit Main context', () => {
  it('should _setUpBodyObserver', () => {
    document.body.innerHTML = '';
    let toExecute: Function;
    const fake = function(cb: Function) {
      toExecute = cb;
      return { disconnect: () => ({}), observe: () => ({}) };
    };
    // @ts-ignore
    Object.defineProperty(window, 'MutationObserver', { value: fake });
    const cb = jest.fn();

    // No main-content element
    mainCtxt._setUpBodyObserver(cb);
    toExecute();
    expect(cb).not.toHaveBeenCalled();

    const el = document.createElement('div');
    el.id = 'main-content';
    document.body.appendChild(el);
    mainCtxt._setUpBodyObserver(cb);
    toExecute();
    expect(cb).toHaveBeenCalled();
  });

  it('should _setUpMainObserver', () => {
    document.body.innerHTML = '';
    let toExecute: Function;
    const observeStub = jest.fn();
    const fake = function(cb: Function) {
      toExecute = cb;
      return { disconnect: () => ({}), observe: observeStub };
    };
    // @ts-ignore
    Object.defineProperty(window, 'MutationObserver', { value: fake });
    const cb = jest.fn();

    const el = document.createElement('div');
    el.id = 'main-content';
    document.body.appendChild(el);
    mainCtxt._setUpMainObserver(cb);
    toExecute();
    expect(cb).toHaveBeenCalled();
    expect(observeStub).toHaveBeenCalled();
    expect(observeStub.mock.calls[0][1]).toEqual({
      childList: true,
      subtree: true,
    });
    expect(observeStub.mock.calls[0][0]).toEqual(
      document.getElementById('main-content'),
    );
  });

  describe('_onDOMChanges', () => {
    it('should call appendKarmaButtonsToPlayers', () => {
      spyOn(matchRoom, 'isReadyECLMatchRoom').and.returnValue(true);
      const spy = spyOn(karmaButtons, 'appendKarmaButtonsToPlayers');
      mainCtxt._onDomChange();
      expect(spy).toHaveBeenCalled();
    });
    it('should call addReportButtonsToPlayers', () => {
      spyOn(matchRoom, 'isReadyECLMatchRoom').and.returnValue(true);
      const spy = spyOn(report, 'addReportButtonsToPlayers');
      mainCtxt._onDomChange();
      expect(spy).toHaveBeenCalled();
    });
    it('should call addKarmaToPlayerBox', () => {
      spyOn(shared, 'isSignedIn').and.returnValue(true);
      const spy = spyOn(karma, 'addKarmaToPlayerBox');
      mainCtxt._onDomChange();
      expect(spy).toHaveBeenCalled();
    });
  });

  it('should handleError', (next) => {
    const listener = (e: CustomEvent) => {
      expect(e.detail).toEqual({ msg: 'Test', type: 99 });
      document.removeEventListener('ECL-2-0-ERROR-MESSAGE', listener);
      next();
    };
    document.addEventListener('ECL-2-0-ERROR-MESSAGE', listener);
    handleError('Test', 99);
  });
});
