import { ECL_ORGA_ID } from '../../config';
import { getDataAboutPlayerFromAngular } from './shared';

export function _isMatchRoom(route: string): boolean {
  const regex = /^https:\/\/(www.)faceit.com\/[a-z]{2}\/csgo\/room\/[0-9a-z\-]+\/?$/;
  return regex.test(route);
}

export function _matchRoomIsSettled(): boolean {
  return !!document.querySelector('match-team-member-v2');
}

export function _isECLRoom(): boolean {
  const nodes = document.querySelectorAll('match-team-member-v2');
  const firstElement = nodes[0];
  if (!firstElement) {
    // something went wrong
    return false;
  }
  const data = getDataAboutPlayerFromAngular(firstElement);
  return data.currentMatch.match.organizerId === ECL_ORGA_ID;
}

export function isReadyECLMatchRoom() {
  if (!_this._isMatchRoom(location.href)) {
    return false;
  }
  if (!_this._matchRoomIsSettled()) {
    return false;
  }
  if (!_this._isECLRoom()) {
    return false;
  }
  return true;
}

export const _this = {
  isReadyECLMatchRoom,
  _isMatchRoom,
  _matchRoomIsSettled,
  _isECLRoom,
};
