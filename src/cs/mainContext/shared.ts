import { AngualarPlayerElementInfo, Player } from '../../types/faceitMainTypes';

export function getDataAboutPlayerFromAngular(
  el: Element,
): AngualarPlayerElementInfo {
  // @ts-ignore
  return angular.element(el).inheritedData().$matchTeamMemberV2Controller;
}

export function genPlayers<T>(arr: Element[]): Array<Player<T>> {
  const players: Array<Player<T>> = [];
  for (const playerElement of arr) {
    const data = _this.getDataAboutPlayerFromAngular(playerElement);
    const player: Player<T> = {
      elements: {} as T,
      guid: data.teamMember.id,
      isUser: data.currentUserGuid === data.teamMember.id,
      matchroom: data.currentMatch.match.id,
      name: data.teamMember.nickname,
    };
    players.push(player);
  }
  return players;
}

export function createButtonElementForBottomBar(): HTMLAnchorElement {
  const el = document.createElement('a');
  el.className = 'match-team-member__controls__button inline-block pull-left';
  el.style.cursor = 'pointer';
  el.style.lineHeight = '30px';
  el.style.width = '20px';
  return el;
}

export function getBottomBarOfTeamMember(node: Element) {
  return node.querySelector('.match-team-member__controls');
}

export function isSignedIn() {
  return document.querySelector('.nickname') !== null;
}

export const _this = {
  genPlayers,
  getDataAboutPlayerFromAngular,
};
