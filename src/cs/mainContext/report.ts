import { ECL_HOST } from '../../config';
import { ReportPlayer } from '../../types/faceitMainTypes';
import {
  createButtonElementForBottomBar,
  genPlayers,
  getBottomBarOfTeamMember,
} from './shared';

export function _createReportURL(
  matchroom: string,
  playerguid: string,
): string {
  const params = new URLSearchParams();
  params.append('matchroom', matchroom);
  params.append('playerguid', playerguid);
  return `${ECL_HOST}/ticket/major?` + params.toString();
}

export function _onReportClick(player: ReportPlayer) {
  const url = _this._createReportURL(player.matchroom, player.guid);
  window.open(url, '_blank');
}

export function addReportButtonsToPlayers() {
  if (document.querySelector('.ecl-2-0-report-button')) {
    return; // already added
  }

  const nodes = document.querySelectorAll('match-team-member-v2');
  const arr = Array.from(nodes);

  const players: ReportPlayer[] = genPlayers<{}>(arr);

  for (let i = 0; i < arr.length; i++) {
    if (players[i].isUser) {
      continue;
    }
    const node = arr[i];
    const parentOfButton = getBottomBarOfTeamMember(node);
    const reportElement = createButtonElementForBottomBar();
    reportElement.innerHTML = '&#9873;';
    reportElement.style.color = 'orange';
    reportElement.title = 'ECL report';
    reportElement.classList.add('ecl-2-0-report-button'); // prevent another call
    reportElement.onclick = () => _this._onReportClick(players[i]);
    parentOfButton.appendChild(reportElement);
  }
}

export const _this = {
  addReportButtonsToPlayers,
  _onReportClick,
  _createReportURL,
};
