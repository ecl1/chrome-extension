import { ECL_HOST } from '../../config';
import {
  KarmaPlayer,
  ShouldHidePlayersRequest,
} from '../../types/faceitMainTypes';
import { FetchResponse, FetchResponsePayload } from '../../types/fetchTypes';
import { MsgErrorResponse } from '../../types/msg';
import { handleError } from './faceit_main_context';
import {
  createButtonElementForBottomBar,
  genPlayers,
  getBottomBarOfTeamMember,
} from './shared';

const _BUTTONS: Array<'upvote' | 'downvote'> = ['upvote', 'downvote'];
const _ARROWS = { upvote: '&#9650', downvote: '&#9660' };
const _COLORS = { upvote: 'greenyellow', downvote: 'red' };

export async function _hideIfShouldNotShowPlayers(players: KarmaPlayer[]) {
  const hidePlayers = await _this._shouldHidePlayers(players);

  for (const player of players) {
    if (!!hidePlayers[player.guid]) {
      _this._hideVoteButtonsFromPlayer(player, hidePlayers[player.guid] as
        | 'upvote'
        | 'downvote');
    }
  }
}

export async function _shouldHidePlayers(
  players: KarmaPlayer[],
): Promise<{ [uuid: string]: string }> {
  const pl: string[] = [];
  players.forEach((p: KarmaPlayer) => pl.push(p.matchroom + p.guid));
  const random = Math.random() * 1000; // prevents race-condition
  const event = new CustomEvent<ShouldHidePlayersRequest>(
    'ECL-2-0-SHOULD-HIDE-PLAYERS-REQ',
    { detail: { random, pl } },
  );
  return new Promise((resolve) => {
    const fn = (ev: CustomEvent<{ [uuid: string]: string }>) => {
      document.removeEventListener(`ECL-2-0-SHOULD-HIDE-PLAYERS-${random}`, fn);
      const matchroom = players[0].matchroom;
      const finalObj: { [uuid: string]: string } = {};
      for (const key in ev.detail) {
        if (!ev.detail.hasOwnProperty(key)) {
          continue;
        }
        finalObj[key.replace(matchroom, '')] = ev.detail[key];
      }
      resolve(finalObj);
    };
    document.addEventListener(`ECL-2-0-SHOULD-HIDE-PLAYERS-${random}`, fn);
    document.dispatchEvent(event);
  });
}

export function _playersContainUser(players: KarmaPlayer[]): boolean {
  return !!players.find((player: KarmaPlayer) => player.isUser);
}

export function _onVoteClick(type: 'upvote' | 'downvote', player: KarmaPlayer) {
  _this._hideVoteButtonsFromPlayer(player, type);
  const random = Math.floor(Math.random() * 10000);
  const reqE = new CustomEvent('ECL-2-0-XHR-REQUEST', {
    detail: JSON.stringify({
      id: random,
      options: {
        body: JSON.stringify({
          turn: type === 'upvote' ? 'up' : 'down',
        }),
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'POST',
      } as RequestInit,
      url: `${ECL_HOST}/api/karma/${player.matchroom}/${player.guid}`,
    }),
  });
  const fn = (e: CustomEvent) => {
    document.removeEventListener(`ECL-2-0-XHR-${random}`, fn);
    _this._onVoteResponse(e, player, type);
  };
  document.addEventListener(`ECL-2-0-XHR-${random}`, fn);
  document.dispatchEvent(reqE);
}

export function _onVoteResponse(
  e: CustomEvent<string>,
  player: KarmaPlayer,
  type: 'upvote' | 'downvote',
) {
  const det = e.detail as string;
  const resp = JSON.parse(det) as FetchResponse;

  if (resp.error) {
    const errData = resp.data as MsgErrorResponse<string>;
    handleError(errData.reason, errData.type);
    _this._showVoteButtonsFromPlayer(player);
    return;
  }
  const data = resp.data as FetchResponsePayload;
  const headers = data.headers as { [k: string]: string };

  if (!headers['content-type'].includes('application/json')) {
    handleError(
      'Request failed, make sure to connect the addon with your ECL account',
    );
    _this._showVoteButtonsFromPlayer(player);
    return;
  }
  const jsonResponse = JSON.parse(data.text) as {
    success: boolean;
    error?: string;
  };
  if (!jsonResponse.success) {
    if (jsonResponse.error === 'Match is not found.') {
      handleError(
        'The match was not found. You can only vote on players when your match has finished. ' +
          'Please note that we need up to two minutes to process finished matches.',
      );
    } else {
      handleError(jsonResponse.error);
    }
    _this._showVoteButtonsFromPlayer(player);
    return;
  }

  document.dispatchEvent(
    new CustomEvent('ECL-2-0-VOTED-FOR-PLAYER', {
      detail: (type === 'upvote' ? 'u' : 'd') + player.matchroom + player.guid,
    }),
  );
}

export function _hideVoteButtonsFromPlayer(
  player: KarmaPlayer,
  type: 'upvote' | 'downvote',
): void {
  try {
    let stillColor: HTMLAnchorElement;
    let greyedOut: HTMLAnchorElement;
    if (type === 'upvote') {
      stillColor = player.elements.upvote;
      greyedOut = player.elements.downvote;
    } else {
      stillColor = player.elements.downvote;
      greyedOut = player.elements.upvote;
    }

    stillColor.onclick = () => ({});
    greyedOut.onclick = () => ({});
    greyedOut.style.filter = 'grayscale(0.8)';
    stillColor.style.filter = 'grayscale(0.2)';
  } catch (e) {
    console.error('Could not remove element, because', e);
  }
}

export function _showVoteButtonsFromPlayer(player: KarmaPlayer): void {
  try {
    player.elements.downvote.style.filter = 'none';
    player.elements.upvote.style.filter = 'none';
    player.elements.downvote.style.color = 'red';
    player.elements.upvote.style.color = 'greenyellow';
    if (!!player.elements.upvote) {
      player.elements.upvote.onclick = () =>
        _this._onVoteClick('upvote', player);
    }
    if (!!player.elements.downvote) {
      player.elements.downvote.onclick = () =>
        _this._onVoteClick('downvote', player);
    }
  } catch (e) {
    console.error('Could not show element, because', e);
  }
}

export function appendKarmaButtonsToPlayers() {
  if (document.querySelector('.ecl-2-0-vote-button')) {
    return; // already added
  }

  const nodes = document.querySelectorAll('match-team-member-v2');
  const arr = Array.from(nodes);

  const players: KarmaPlayer[] = genPlayers(arr);

  if (!_this._playersContainUser(players)) {
    return; // #23
  }

  for (let it = 0; it < arr.length; it++) {
    if (players[it].isUser) {
      continue;
    }
    const node = arr[it];
    const parentOfButton = getBottomBarOfTeamMember(node);

    for (const button of _BUTTONS) {
      const el = createButtonElementForBottomBar(); // new button
      el.title = 'ECL karma ' + button; // tooltip
      el.innerHTML = _ARROWS[button]; // set text
      el.style.color = _COLORS[button];
      el.classList.add('ecl-2-0-vote-button'); // stop recursion
      el.onclick = () => _this._onVoteClick(button, players[it]);
      players[it].elements[button] = el; // add to player
      parentOfButton.appendChild(el); // add to DOM
    }
  }
  _this._hideIfShouldNotShowPlayers(players);
}

export const _this = {
  _shouldHidePlayers,
  _hideIfShouldNotShowPlayers,
  _hideVoteButtonsFromPlayer,
  _onVoteClick,
  _onVoteResponse,
  _showVoteButtonsFromPlayer,
  _playersContainUser,
  appendKarmaButtonsToPlayers,
};
