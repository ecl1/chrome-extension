import * as shared from './shared';
import Spy = jasmine.Spy;
import { ECL_HOST } from '../../config';
import { ReportPlayer } from '../../types/faceitMainTypes';
import {
  _createReportURL,
  _onReportClick,
  _this as reportWrapper,
  addReportButtonsToPlayers,
} from './report';

describe('report', () => {
  let genPlayersSpy: Spy;

  beforeEach(() => {
    genPlayersSpy = spyOn(shared, 'genPlayers');
  });

  describe('addReportButtonsToPlayers()', () => {
    it('should not add another element if report button found in DOM', () => {
      // Set up
      document.body.innerHTML = '';
      const el = document.createElement('a');
      el.className = 'ecl-2-0-report-button';
      document.body.appendChild(el);

      // call
      addReportButtonsToPlayers();

      // test
      expect(genPlayersSpy).not.toHaveBeenCalled();

      // clean
      document.body.innerHTML = '';
    });

    it('should add report buttons', () => {
      // set up
      document.body.innerHTML = '';
      const node1 = document.createElement('match-team-member-v2');
      const node2 = document.createElement('match-team-member-v2');
      const bot1 = document.createElement('div');
      const bot2 = document.createElement('div');
      bot1.className = 'match-team-member__controls';
      bot2.className = 'match-team-member__controls';
      node1.appendChild(bot1);
      node2.appendChild(bot2);
      document.body.appendChild(node1);
      document.body.appendChild(node2);
      genPlayersSpy.and.returnValue([
        { isUser: true, elements: {} },
        { isUser: false, elements: {} },
      ]);
      reportWrapper._onReportClick = jest.fn();

      // call
      addReportButtonsToPlayers();

      // test
      expect(bot1.childElementCount).toEqual(0); // isuser === true
      expect(bot2.childElementCount).toEqual(1); // isuser === false
      const reportButtons = document.querySelectorAll('.ecl-2-0-report-button');
      expect(reportButtons.length).toEqual(1);

      (reportButtons[0] as HTMLAnchorElement).onclick(null);
      expect(reportWrapper._onReportClick).toHaveBeenCalled();
    });
  });

  describe('_onReportClick()', () => {
    it('should open link to report page', () => {
      // set up
      reportWrapper._createReportURL = jest.fn().mockReturnValue('THEURL');
      const spy = spyOn(window, 'open');

      // call
      _onReportClick({
        guid: 'guidddddd',
        matchroom: 'matchrooooom',
      } as ReportPlayer);

      // test
      expect(reportWrapper._createReportURL).toHaveBeenLastCalledWith(
        'matchrooooom',
        'guidddddd',
      );
      expect(spy).toHaveBeenLastCalledWith('THEURL', '_blank');
    });
  });

  describe('_onReportClick()', () => {
    it('should return valid URLs', () => {
      expect(_createReportURL('MATCHROOM', 'PLAYER')).toEqual(
        `${ECL_HOST}/ticket/major?matchroom=MATCHROOM&playerguid=PLAYER`,
      );
      expect(_createReportURL('MATCHR?OM', 'P&A#ER')).toEqual(
        `${ECL_HOST}/ticket/major?matchroom=MATCHR%3FOM&playerguid=P%26A%23ER`,
      );
    });
  });
});
