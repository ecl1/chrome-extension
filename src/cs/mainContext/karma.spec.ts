import { ECL_HOST } from '../../config';
import {
  _fetchKarma,
  _getTPImgUrl,
  _onKarmaResponse,
  _setUpRefresh,
  _this as karmaWrapper,
  addKarmaToPlayerBox,
} from './karma';

describe('Karma', () => {
  describe('_getTPImgUrl()', () => {
    it('should return image url', () => {
      (window as any).ECL_2_0_URL = 'chrome://web-ext/';
      expect(_getTPImgUrl()).toEqual('chrome://web-ext/assets/tp.webp');
    });
  });

  describe('_setUpRefresh()', () => {
    it('should remove el and call addKarmaToPLayerBox every 15 minutes', () => {
      global.setTimeout = jest.fn();
      karmaWrapper.addKarmaToPlayerBox = jest.fn();

      _setUpRefresh();
      expect((global.setTimeout as jest.Mock).mock.calls[0][1]).toEqual(
        15 * 60 * 1000,
      );
      expect(karmaWrapper.addKarmaToPlayerBox).not.toHaveBeenCalled();

      document.body.innerHTML = '';
      const el = document.createElement('span');
      el.className = 'ecl-2-0-nickname-karma';
      document.body.appendChild(el);
      (global.setTimeout as jest.Mock).mock.calls[0][0]();

      expect(karmaWrapper.addKarmaToPlayerBox).toHaveBeenCalled();
      expect(document.querySelector('.ecl-2-0-nickname-karma')).toEqual(null);

      (global.setTimeout as jest.Mock).mock.calls[0][0]();
    });
  });

  describe('addKarmaToPlayerBox()', () => {
    it('should not add TP again if it already exists on DOM', () => {
      document.body.innerHTML = '';
      const el = document.createElement('span');
      el.className = 'ecl-2-0-nickname-karma';
      document.body.appendChild(el);
      karmaWrapper._fetchKarma = jest.fn();

      addKarmaToPlayerBox();

      expect(karmaWrapper._fetchKarma).not.toHaveBeenCalled();
    });
    it('should add TP to nickname', () => {
      document.body.innerHTML = '';
      const el = document.createElement('span');
      el.className = 'nickname';
      el.innerHTML = 'FoseFx';
      document.body.appendChild(el);
      karmaWrapper._fetchKarma = jest.fn();
      karmaWrapper._setUpRefresh = jest.fn();

      addKarmaToPlayerBox();

      expect(karmaWrapper._fetchKarma).toHaveBeenCalled();
      expect(karmaWrapper._setUpRefresh).toHaveBeenCalled();
      expect(el.childElementCount).toEqual(1);
    });
  });

  describe('_onKarmaResponse()', () => {
    it('should add karma to karmaWrapper and show it', () => {
      document.body.innerHTML = '';
      const el = document.createElement('span');
      el.className = 'ecl-2-0-nickname-karma';
      document.body.appendChild(el);
      const e = {
        detail: JSON.stringify({
          data: { text: JSON.stringify({ user: { karma: 1234 } }) },
          error: false,
        }),
      } as CustomEvent;
      _onKarmaResponse(e);

      expect(el.style.display).toEqual('inline');
      expect(el.innerHTML).toContain('1234');
    });
  });

  describe('_fetchKarma()', () => {
    it('fetch karma and call _onKarmaResponse', (next) => {
      karmaWrapper._onKarmaResponse = jest.fn();
      const fn = (e: CustomEvent) => {
        document.removeEventListener('ECL-2-0-XHR-REQUEST', fn);

        const detail = JSON.parse(e.detail);

        expect(detail.url).toEqual(`${ECL_HOST}/api/session/me`);
        expect(detail.options.method).toEqual(`GET`);

        const event = new CustomEvent(`ECL-2-0-XHR-${detail.id}`, {
          detail: 'DETAILLOL',
        });
        document.dispatchEvent(event);
        expect(karmaWrapper._onKarmaResponse).toHaveBeenCalledWith(event);
        next();
      };
      document.addEventListener('ECL-2-0-XHR-REQUEST', fn);
      _fetchKarma();
    });
  });
});
