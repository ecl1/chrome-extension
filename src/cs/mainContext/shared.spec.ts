import { getDataAboutPlayerFromAngular, isSignedIn } from './shared';
import { _this as sharedWrapper } from './shared';

describe('shared', () => {
  it('should getDataAboutPlayerFromAngular', () => {
    // @ts-ignore
    global.angular = {
      element: () => ({
        inheritedData: () => ({
          $matchTeamMemberV2Controller: 'tsetStringInheritedData',
        }),
      }),
    };
    expect(
      getDataAboutPlayerFromAngular(({} as unknown) as HTMLElement),
    ).toEqual('tsetStringInheritedData');
  });

  it('should genPlayers', () => {
    const elements = ([
      {
        currentMatch: { match: { id: 1 } },
        currentUserGuid: 1,
        teamMember: { id: 1, nickname: 'player1' },
      },
    ] as unknown) as Element[];
    sharedWrapper.getDataAboutPlayerFromAngular = jest
      .fn()
      .mockImplementation((el: any) => el);
    expect(sharedWrapper.genPlayers(elements)).toEqual([
      {
        elements: { upvote: undefined, downvote: undefined },
        guid: 1,
        isUser: true,
        matchroom: 1,
        name: 'player1',
      },
    ]);
  });

  describe('isSignedIn()', () => {
    it('should return if isSignedIn into faceit', () => {
      document.body.innerHTML = '';

      expect(isSignedIn()).toEqual(false);

      const el = document.createElement('span');
      el.className = 'nickname';
      document.body.appendChild(el);

      expect(isSignedIn()).toEqual(true);
    });
  });
});
