import { ECL_HOST } from '../../config';
import { KarmaPlayer } from '../../types/faceitMainTypes';
import * as mainContext from './faceit_main_context';
import {
  _hideVoteButtonsFromPlayer,
  _onVoteResponse,
  _playersContainUser,
  _shouldHidePlayers,
  _showVoteButtonsFromPlayer,
} from './karmaButtons';
import { _this as karmaButtonsWrapper } from './karmaButtons';
import * as shared from './shared';
import Spy = jasmine.Spy;

describe('karmaButtons', () => {
  let genPlayersSpy: Spy;
  let handleErrorSpy: Spy;

  beforeEach(() => {
    genPlayersSpy = spyOn(shared, 'genPlayers');
    handleErrorSpy = spyOn(mainContext, 'handleError');
  });

  it('should return if _shouldHidePlayers', async () => {
    const matchroom = 'MATCHROOM';
    const players = [
      { matchroom, guid: 'PLAYER1' },
      { matchroom, guid: 'PLAYER2' },
      { matchroom, guid: 'PLAYER3' },
    ] as KarmaPlayer[];

    let onReqCalled = false;
    const onReq = (e: CustomEvent<{ random: number }>) => {
      onReqCalled = true;
      document.removeEventListener('ECL-2-0-SHOULD-HIDE-PLAYERS-REQ', onReq);
      const ev = new CustomEvent(
        'ECL-2-0-SHOULD-HIDE-PLAYERS-' + e.detail.random,
        {
          detail: {
            MATCHROOMPLAYER1: false,
            MATCHROOMPLAYER2: true,
            MATCHROOMPLAYER3: true,
          },
        },
      );
      document.dispatchEvent(ev);
    };
    document.addEventListener('ECL-2-0-SHOULD-HIDE-PLAYERS-REQ', onReq);

    const res = await _shouldHidePlayers(players);
    expect(res).toEqual({
      PLAYER1: false,
      PLAYER2: true,
      PLAYER3: true,
    });
    expect(onReqCalled).toEqual(true);
  });

  it('should _hideIfShouldNotShowPlayers', async () => {
    const hiddenPlayers = {
      PLAYER1: 'upvote',
      PLAYER2: 'downvote',
      PLAYER3: '',
    };
    const players = [
      { guid: 'PLAYER1' },
      { guid: 'PLAYER2' },
      { guid: 'PLAYER3' },
    ] as KarmaPlayer[];
    karmaButtonsWrapper._shouldHidePlayers = jest
      .fn()
      .mockResolvedValue(hiddenPlayers);
    karmaButtonsWrapper._hideVoteButtonsFromPlayer = jest.fn();
    await karmaButtonsWrapper._hideIfShouldNotShowPlayers(players);
    expect(
      karmaButtonsWrapper._hideVoteButtonsFromPlayer,
    ).toHaveBeenCalledTimes(2);
  });

  it('should return whether _playersContainUser()', () => {
    expect(_playersContainUser([])).toEqual(false);
    expect(
      _playersContainUser([({ isUser: false } as unknown) as KarmaPlayer]),
    ).toEqual(false);
    expect(
      _playersContainUser([
        ({ isUser: true } as unknown) as KarmaPlayer,
        ({ isUser: false } as unknown) as KarmaPlayer,
      ]),
    ).toEqual(true);
  });

  describe('_onVoteClick()', () => {
    let player: KarmaPlayer;

    beforeEach(() => {
      karmaButtonsWrapper._hideVoteButtonsFromPlayer = jest.fn();

      player = {
        guid: 'PlayerGUID',
        matchroom: 'matchroom',
      } as KarmaPlayer;
    });

    it('should hide player', () => {
      karmaButtonsWrapper._onVoteClick('upvote', player);
      expect(
        karmaButtonsWrapper._hideVoteButtonsFromPlayer,
      ).toHaveBeenLastCalledWith(player, 'upvote');

      karmaButtonsWrapper._onVoteClick('downvote', player);
      expect(
        karmaButtonsWrapper._hideVoteButtonsFromPlayer,
      ).toHaveBeenLastCalledWith(player, 'downvote');
    });

    it('should send upvote request _onVoteClick()', (next) => {
      const listener = (event: CustomEvent) => {
        const det = JSON.parse(event.detail);
        expect(det.url).toEqual(
          `${ECL_HOST}/api/karma/${player.matchroom}/${player.guid}`,
        );
        expect(JSON.parse(det.options.body)).toEqual({ turn: 'up' });
        document.removeEventListener('ECL-2-0-XHR-REQUEST', listener);
        next();
      };
      document.addEventListener('ECL-2-0-XHR-REQUEST', listener);
      karmaButtonsWrapper._onVoteClick('upvote', player);
    });

    it('should send downvote request _onVoteClick()', (next) => {
      const listener = (event: CustomEvent) => {
        const det = JSON.parse(event.detail);
        expect(det.url).toEqual(
          `${ECL_HOST}/api/karma/${player.matchroom}/${player.guid}`,
        );
        expect(JSON.parse(det.options.body)).toEqual({ turn: 'down' });
        document.removeEventListener('ECL-2-0-XHR-REQUEST', listener);
        next();
      };
      document.addEventListener('ECL-2-0-XHR-REQUEST', listener);
      karmaButtonsWrapper._onVoteClick('downvote', player);
    });

    it('should call _onVoteResponse', (next) => {
      karmaButtonsWrapper._onVoteResponse = jest.fn();

      const listener = (event: CustomEvent) => {
        const det = JSON.parse(event.detail);
        document.removeEventListener('ECL-2-0-XHR-REQUEST', listener);
        const respEvent = new CustomEvent(`ECL-2-0-XHR-${det.id}`, {
          detail: 'testDetail',
        });
        document.dispatchEvent(respEvent);
        setTimeout(() => {
          expect(karmaButtonsWrapper._onVoteResponse).toHaveBeenLastCalledWith(
            respEvent,
            player,
            'downvote',
          );
          next();
        }, 100);
      };
      document.addEventListener('ECL-2-0-XHR-REQUEST', listener);
      karmaButtonsWrapper._onVoteClick('downvote', player);
    });
  });

  describe('onVoteResponse', () => {
    beforeEach(() => {
      karmaButtonsWrapper._showVoteButtonsFromPlayer = jest.fn();
    });
    it('should handle network errors in _onVoteResponse', () => {
      _onVoteResponse(
        ({
          detail: JSON.stringify({
            data: {
              reason: 'Some Reason',
              type: 999,
            },
            error: true,
          }),
        } as unknown) as CustomEvent<string>,
        null,
        undefined,
      );
      expect(handleErrorSpy).toHaveBeenLastCalledWith('Some Reason', 999);
      expect(karmaButtonsWrapper._showVoteButtonsFromPlayer).toHaveBeenCalled();
    });

    it('should handle content-type errors in _onVoteResponse', () => {
      _onVoteResponse(
        ({
          detail: JSON.stringify({
            data: {
              headers: { 'content-type': 'plain/html; charset=utf-8' },
            },
            error: false,
          }),
        } as unknown) as CustomEvent<string>,
        null,
        undefined,
      );
      expect(handleErrorSpy).toHaveBeenLastCalledWith(
        'Request failed, make sure to connect the addon with your ECL account',
      );
      expect(karmaButtonsWrapper._showVoteButtonsFromPlayer).toHaveBeenCalled();
    });

    it('should handle no errors in _onVoteResponse', () => {
      const onVotedEvent = jest.fn();
      document.addEventListener('ECL-2-0-VOTED-FOR-PLAYER', onVotedEvent);
      _onVoteResponse(
        ({
          detail: JSON.stringify({
            data: {
              headers: { 'content-type': 'application/json' },
              text: JSON.stringify({ success: true }),
            },
            error: false,
          }),
        } as unknown) as CustomEvent<string>,
        {
          elements: {
            downvote: ({
              style: {
                setProperty: jest.fn(),
              },
            } as unknown) as HTMLElement,
            upvote: ({
              style: {
                setProperty: jest.fn(),
              },
            } as unknown) as HTMLElement,
          },
        } as KarmaPlayer,
        undefined,
      );
      expect(handleErrorSpy).not.toHaveBeenCalled();
      expect(onVotedEvent).toHaveBeenCalled();
      document.removeEventListener('ECL-2-0-VOTED-FOR-PLAYER', onVotedEvent);
    });

    it('should handle match-not-found-error in _onVoteResponse', () => {
      _onVoteResponse(
        ({
          detail: JSON.stringify({
            data: {
              headers: { 'content-type': 'application/json' },
              text: JSON.stringify({
                error: 'Match is not found.',
                success: false,
              }),
            },
            error: false,
          }),
        } as unknown) as CustomEvent<string>,
        null,
        undefined,
      );
      expect(handleErrorSpy).toHaveBeenLastCalledWith(
        'The match was not found. You can only vote on players when your match has finished. ' +
          'Please note that we need up to two minutes to process finished matches.',
      );
      expect(karmaButtonsWrapper._showVoteButtonsFromPlayer).toHaveBeenCalled();
    });

    it('should handle server sent errors in _onVoteResponse', () => {
      _onVoteResponse(
        ({
          detail: JSON.stringify({
            data: {
              headers: { 'content-type': 'application/json' },
              text: JSON.stringify({
                error: 'Something has failed.',
                success: false,
              }),
            },
            error: false,
          }),
        } as unknown) as CustomEvent<string>,
        null,
        undefined,
      );
      expect(handleErrorSpy).toHaveBeenLastCalledWith('Something has failed.');
      expect(karmaButtonsWrapper._showVoteButtonsFromPlayer).toHaveBeenCalled();
    });
  });

  it('should _hideVoteButtonsFromPlayer()', () => {
    document.body.innerHTML = '';
    const upEl = document.createElement('a') as HTMLAnchorElement;
    const downEl = document.createElement('a') as HTMLAnchorElement;
    document.body.appendChild(upEl);
    document.body.appendChild(downEl);
    const playerMock = ({
      elements: {
        downvote: downEl,
        upvote: upEl,
      },
    } as unknown) as KarmaPlayer;
    _hideVoteButtonsFromPlayer(playerMock, 'upvote');
    expect(upEl.style.filter).toEqual('grayscale(0.2)');
    expect(downEl.style.filter).toEqual('grayscale(0.8)');

    _hideVoteButtonsFromPlayer(playerMock, 'downvote');
    expect(downEl.style.filter).toEqual('grayscale(0.2)');
    expect(upEl.style.filter).toEqual('grayscale(0.8)');

    playerMock.elements = {} as any;
    console.error = jest.fn();
    _hideVoteButtonsFromPlayer(playerMock, null); // should console error
    expect(console.error).toHaveBeenCalled();
  });

  it('should _showVoteButtonsFromPlayer', () => {
    const p = ({
      elements: {
        downvote: {
          style: {
            color: undefined,
            filter: undefined,
          },
        },
        upvote: {
          style: {
            color: undefined,
            filter: undefined,
          },
        },
      },
    } as unknown) as KarmaPlayer;
    _showVoteButtonsFromPlayer(p);
    expect(p.elements.downvote.style.filter).toEqual('none');
    expect(p.elements.upvote.style.filter).toEqual('none');
    expect(p.elements.downvote.style.color).toEqual('red');
    expect(p.elements.upvote.style.color).toEqual('greenyellow');
    console.error = jest.fn();
    _showVoteButtonsFromPlayer(null);
    expect(console.error).toHaveBeenCalled();
  });

  describe('appendKarmaButtonsToPlayers', () => {
    it('should not add buttons to dom, if they already exist', () => {
      document.body.innerHTML = '';
      const el = document.createElement('a');
      el.classList.add('ecl-2-0-vote-button');
      document.body.appendChild(el);
      karmaButtonsWrapper.appendKarmaButtonsToPlayers();
      const nNodes = document.getElementsByClassName('ecl-2-0-vote-button')
        .length;
      expect(nNodes).toEqual(1);
    });

    it('should add buttons to dom', () => {
      document.body.innerHTML = '';
      const tm1 = document.createElement('match-team-member-v2');
      const tm1c = document.createElement('div');
      const tm2 = document.createElement('match-team-member-v2');
      const tm2c = document.createElement('div');
      tm1c.classList.add('match-team-member__controls');
      tm2c.classList.add('match-team-member__controls');
      tm1.appendChild(tm1c);
      tm2.appendChild(tm2c);
      document.body.appendChild(tm1);
      document.body.appendChild(tm2);

      karmaButtonsWrapper._onVoteClick = jest.fn();
      karmaButtonsWrapper._hideIfShouldNotShowPlayers = jest.fn();
      genPlayersSpy.and.returnValue([
        { elements: {}, isUser: false },
        { elements: {}, isUser: true },
      ]);
      karmaButtonsWrapper._playersContainUser = jest.fn().mockReturnValue(true);

      karmaButtonsWrapper.appendKarmaButtonsToPlayers();
      expect(tm1c.childElementCount).not.toEqual(0);
      expect(tm2c.childElementCount).toEqual(0);
      expect(
        karmaButtonsWrapper._hideIfShouldNotShowPlayers,
      ).toHaveBeenCalled();

      // test if onclick is set
      const el = document.querySelector(
        '.ecl-2-0-vote-button',
      ) as HTMLAnchorElement;
      el.onclick(null);
      expect(karmaButtonsWrapper._onVoteClick).toHaveBeenCalled();
    });

    it('should not add buttons to dom if user is not a player', () => {
      document.body.innerHTML = '';
      const tm1 = document.createElement('match-team-member-v2');
      const tm1c = document.createElement('div');
      tm1c.classList.add('match-team-member__controls');
      tm1.appendChild(tm1c);
      document.body.appendChild(tm1);

      genPlayersSpy.and.returnValue([]);
      karmaButtonsWrapper._playersContainUser = jest
        .fn()
        .mockReturnValue(false);

      karmaButtonsWrapper.appendKarmaButtonsToPlayers();
      expect(tm1c.childElementCount).toEqual(0);
    });
  });
});
