import { AngualarPlayerElementInfo } from '../../types/faceitMainTypes';
import { addKarmaToPlayerBox } from './karma';
import { appendKarmaButtonsToPlayers } from './karmaButtons';
import { isReadyECLMatchRoom } from './matchRoom';
import { addReportButtonsToPlayers } from './report';
import { isSignedIn } from './shared';

/**
 * We are waiting for faceit to load it's
 * interface and then call the callback
 */
export function _setUpBodyObserver(callback: () => any) {
  const initialObserver = new MutationObserver(() => {
    if (document.getElementById('main-content')) {
      initialObserver.disconnect();
      callback();
    }
  });
  initialObserver.observe(document.body, { childList: true, subtree: true });
}

/**
 * This is the main loop
 * We wait for any DOM-Changes
 * and run callback every time
 */
export function _setUpMainObserver(callback: Function) {
  const obs = new MutationObserver((records: MutationRecord[]) => {
    callback();
  });
  const faceitMainContent = document.getElementById('main-content');
  obs.observe(faceitMainContent, { childList: true, subtree: true });
}

/**
 * This function is called every time
 * the DOM changes.
 */
export async function _onDomChange() {
  if (isSignedIn()) {
    addKarmaToPlayerBox();
  }
  if (isReadyECLMatchRoom()) {
    appendKarmaButtonsToPlayers();
    addReportButtonsToPlayers();
  }
}

export function handleError(msg: string, type?: number): void {
  const event = new CustomEvent('ECL-2-0-ERROR-MESSAGE', {
    detail: { msg, type },
  });
  document.dispatchEvent(event);
}

_setUpBodyObserver(() => _setUpMainObserver(_onDomChange));
_onDomChange();
