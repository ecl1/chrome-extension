import { ECL_ORGA_ID } from '../../config';
import { _isECLRoom, _isMatchRoom, _matchRoomIsSettled } from './matchRoom';
import { _this as matchRoomWrapper } from './matchRoom';
import * as sharedContext from './shared';

describe('matchRoom', () => {
  it('should detect (csgo) faceit matchrooms', () => {
    expect(_isMatchRoom('https://www.faceit.com/en')).toEqual(false);
    expect(
      _isMatchRoom(
        'https://www.faceit.com/de/csgo/room/1-38f7b42b-ffc3-4dda-920a-9137086e9022/scoreboard',
      ),
    ).toEqual(false);
    expect(
      _isMatchRoom(
        'https://www.faceit.com/de/csgo/room/1-38f7b42b-ffc3-4dda-920a-9137086e9022',
      ),
    ).toEqual(true);
  });

  it('should check if _matchRoomIsSettled', () => {
    document.body.innerHTML = '';
    expect(_matchRoomIsSettled()).toEqual(false);
    document.body.appendChild(document.createElement('match-team-member-v2'));
    expect(_matchRoomIsSettled()).toEqual(true);
    document.body.innerHTML = '';
  });

  it('should detect whether isECLRoom', () => {
    document.body.innerHTML = '';
    // Invalid state, fails silently
    expect(_isECLRoom()).toEqual(false);
    document.body.appendChild(document.createElement('match-team-member-v2'));
    const spy = spyOn(
      sharedContext,
      'getDataAboutPlayerFromAngular',
    ).and.returnValue({ currentMatch: { match: { organizerId: 'SthElse' } } });
    expect(_isECLRoom()).toEqual(false);
    spy.and.returnValue({
      currentMatch: { match: { organizerId: ECL_ORGA_ID } },
    });
    expect(_isECLRoom()).toEqual(true);
  });

  it('should return if isReadyECLMatchRoom', () => {
    // TRUE
    matchRoomWrapper._isMatchRoom = jest.fn().mockReturnValue(true);
    matchRoomWrapper._matchRoomIsSettled = jest.fn().mockReturnValue(true);
    matchRoomWrapper._isECLRoom = jest.fn().mockReturnValue(true);
    expect(matchRoomWrapper.isReadyECLMatchRoom()).toEqual(true);

    // FALSE
    matchRoomWrapper._isMatchRoom = jest.fn().mockReturnValue(true);
    matchRoomWrapper._matchRoomIsSettled = jest.fn().mockReturnValue(true);
    matchRoomWrapper._isECLRoom = jest.fn().mockReturnValue(false);
    expect(matchRoomWrapper.isReadyECLMatchRoom()).toEqual(false);

    matchRoomWrapper._isMatchRoom = jest.fn().mockReturnValue(true);
    matchRoomWrapper._matchRoomIsSettled = jest.fn().mockReturnValue(false);
    expect(matchRoomWrapper.isReadyECLMatchRoom()).toEqual(false);

    matchRoomWrapper._isMatchRoom = jest.fn().mockReturnValue(false);
    expect(matchRoomWrapper.isReadyECLMatchRoom()).toEqual(false);
  });
});
