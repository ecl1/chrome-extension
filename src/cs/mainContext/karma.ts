import { ECL_HOST } from '../../config';

export function _getTPImgUrl(): string {
  const host = (window as any).ECL_2_0_URL;
  return `${host}assets/tp.webp`;
}

export function _fetchKarma() {
  const random = Math.floor(Math.random() * 10000);
  const reqE = new CustomEvent('ECL-2-0-XHR-REQUEST', {
    detail: JSON.stringify({
      id: random,
      options: {
        method: 'GET',
      } as RequestInit,
      url: `${ECL_HOST}/api/session/me`,
    }),
  });
  const fn = (e: CustomEvent) => {
    document.removeEventListener(`ECL-2-0-XHR-${random}`, fn);
    _this._onKarmaResponse(e);
  };
  document.addEventListener(`ECL-2-0-XHR-${random}`, fn);
  document.dispatchEvent(reqE);
}

// errors fail silently as there is no user interaction
// needed for this request to happen
export function _onKarmaResponse(e: CustomEvent) {
  const detail = JSON.parse(e.detail) as {
    error: boolean;
    data?: { text: string };
  };
  if (detail.error) {
    return;
  }
  const user = JSON.parse(detail.data.text).user;
  const karma = user.karma;

  const wrapper = document.querySelector(
    '.ecl-2-0-nickname-karma',
  ) as HTMLElement;
  const el = document.createElement('span');
  el.innerHTML = karma;
  el.title = 'Your ECL Karma';
  wrapper.appendChild(el);
  wrapper.style.display = 'inline';
}

/** Updates the karma every 15 minutes */
export function _setUpRefresh() {
  setTimeout(() => {
    const el = document.querySelector('.ecl-2-0-nickname-karma');
    if (el !== null) {
      el.remove();
      _this.addKarmaToPlayerBox();
    }
  }, 1000 * 60 * 15);
}

export function addKarmaToPlayerBox() {
  if (document.querySelector('.ecl-2-0-nickname-karma') !== null) {
    return;
  }
  const nicknameEl = document.querySelector('.nickname') as HTMLElement;
  const karmaWrapperEl = document.createElement('span');
  karmaWrapperEl.innerHTML = '&nbsp;';
  karmaWrapperEl.style.fontSize = '12px';

  const karmaImgEl = document.createElement('img') as HTMLImageElement;
  karmaImgEl.alt = 'ECL TP';
  karmaImgEl.src = _this._getTPImgUrl();
  karmaImgEl.height = 25;
  karmaImgEl.width = 25;
  karmaImgEl.title = 'Your ECL Karma';

  karmaWrapperEl.appendChild(karmaImgEl);
  karmaWrapperEl.className = 'ecl-2-0-nickname-karma';
  karmaWrapperEl.style.display = 'none'; // hide until data fetched
  nicknameEl.appendChild(karmaWrapperEl);
  _this._fetchKarma();
  _this._setUpRefresh();
}

export const _this = {
  _getTPImgUrl,
  _fetchKarma,
  _onKarmaResponse,
  _setUpRefresh,
  addKarmaToPlayerBox,
};
