import '../../CHANGELOG.md';
import './styles/popup.scss';

import { ECL_HOST, STORE_VARS } from '../config';
import * as storage from '../shared/storage';

export function onLoad() {
  const versionElement = document.getElementById('ecl-2-0-version');
  if (versionElement) {
    versionElement.innerHTML = `${ECLAVERSION} ${ECLABRANCH} (${ECLASHA})`;
  }
  _this.checkIfAuthed();
}

async function checkIfAuthed() {
  if (!(await storage.isTokenNeeded())) {
    return;
  }
  const hasNotAuthedSection = document.getElementById('has-not-authed-section');
  if (hasNotAuthedSection) {
    const profileAnchor = document.getElementById(
      'profile-anchor',
    ) as HTMLAnchorElement;
    if (profileAnchor) {
      profileAnchor.href = `${ECL_HOST}/profile`;
    }
    hasNotAuthedSection.style.display = 'block';
  }
  setTimeout(_this.checkIfAuthed, 2000);
}

export const _this = {
  checkIfAuthed,
  onLoad,
};

if (typeof ECLAVERSION !== 'undefined') {
  onLoad();
}
