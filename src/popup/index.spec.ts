jest.mock('../../CHANGELOG.md', () => jest.fn());
jest.mock('./styles/popup.scss', () => jest.fn());
import { ECL_HOST } from '../config';
import * as storage from '../shared/storage';
import { _this as indexWrapper } from './index';

describe('popup/index.ts', () => {
  it('should checkIfAuthed()', async () => {
    global.setTimeout = jest.fn();
    document.body.innerHTML = '';
    const el = document.createElement('div');
    el.id = 'has-not-authed-section';
    document.body.appendChild(el);
    const el2 = document.createElement('a') as HTMLAnchorElement;
    el2.id = 'profile-anchor';
    document.body.appendChild(el2);

    const spy = spyOn(storage, 'isTokenNeeded').and.returnValue(
      Promise.resolve(true),
    );

    await indexWrapper.checkIfAuthed();

    expect(global.setTimeout).toHaveBeenLastCalledWith(
      indexWrapper.checkIfAuthed,
      2000,
    );
    expect(el.style.display).toEqual('block');
    expect(el2.href).toEqual(`${ECL_HOST}/profile`);

    spy.and.returnValue(Promise.resolve(false));
    global.setTimeout = jest.fn();

    await indexWrapper.checkIfAuthed();
    expect(global.setTimeout).not.toHaveBeenCalled();
  });
  it('should run onLoad()', () => {
    // @ts-ignore
    global.ECLAVERSION = '1234';
    // @ts-ignore
    global.ECLABRANCH = 'stable';
    // @ts-ignore
    global.ECLASHA = 'h4shv4lu';

    document.body.innerHTML = '';
    const el = document.createElement('span');
    el.id = 'ecl-2-0-version';
    document.body.appendChild(el);
    indexWrapper.checkIfAuthed = jest.fn();

    indexWrapper.onLoad();

    expect(el.textContent.trim()).toEqual(
      `${ECLAVERSION} ${ECLABRANCH} (${ECLASHA})`,
    );
    expect(indexWrapper.checkIfAuthed).toHaveBeenCalled();

    // @ts-ignore
    delete global.ECLAVERSION;
    // @ts-ignore
    delete global.ECLABRANCH;
    // @ts-ignore
    delete global.ECLASHA;
  });
});
