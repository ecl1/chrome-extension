export interface AngualarPlayerElementInfo {
  currentUserGuid: string;
  teamMember: {
    id: string;
    nickname: string;
    avatar: string;
  };
  currentMatch: {
    match: {
      organizerId: string;
      entity: {
        name: string;
      };
      id: string;
    };
  };
}

// Note: each subroutine
// e.g. karma buttons, report buttons
// Have their own instances of their players
// so report button's players dont have up/donwvote set
export interface Player<T> {
  guid: string;
  matchroom: string;
  name: string;
  isUser: boolean;
  elements: T;
}

export interface KarmaPlayerElements {
  downvote?: HTMLAnchorElement;
  upvote?: HTMLAnchorElement;
}

export type KarmaPlayer = Player<KarmaPlayerElements>;
export type ReportPlayer = Player<{}>;

export interface ShouldHidePlayersRequest {
  random: number;
  pl: string[];
}
