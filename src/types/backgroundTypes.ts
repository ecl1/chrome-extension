export interface OnInstalledDetails {
  reason: browser.runtime.OnInstalledReason;
  previousVersion?: string;
  id?: string;
}
