import { MsgRequest, MsgResponse } from './msg';

export interface FetchRequestOptions {
  url: string;
  options: RequestInit;
}

export interface FetchRequest extends MsgRequest<FetchRequestOptions> {}

export type FetchResponse = MsgResponse<string | FetchResponsePayload>;

export interface FetchResponsePayload {
  headers: any;
  ok: boolean;
  status: number; // e.g. 200, 404
  url: string;
  text: string;
}

export interface TokenResponse {
  userId: number;
  token: string;
}
