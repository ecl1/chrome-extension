// Types related to bg-cs messaging

export interface MsgResponse<T> {
  error: boolean;
  data: MsgErrorResponse<T> | T;
}

export interface MsgErrorResponse<T> {
  type: MsgErrorType;
  reason: T;
}

export interface MsgRequest<T> {
  type: MsgRequestType;
  payload: T;
}

export enum MsgErrorType {
  TypeNotKnown,
  NotImplementedYet,
  FetchError,
}

export enum MsgRequestType {
  FetchRequest,
}
