
<div align="center">
    <h1>ECL 2.0 (WebExtension)</h1>
</div>

<div align="center">
    <img src="https://gitlab.com/ecl1/chrome-extension/raw/master/docs/ecl.webp" alt="ECL 2.0 logo"/>
    <br>
    <i>Integrates ECL website features into your matches</i>
</div>
<br><br>

<div align="center">
    <h2>Features</h2>
</div>

<div align="center">
    <img src="https://gitlab.com/ecl1/chrome-extension/raw/master/docs/arrows.svg" alt="Karma arrows"/>
    <h3>Karma Integration</h3>
</div>

We noticed most players do not take the time to upvote good and downvote bad behavior. If you don't know yet, ECL has their own karma system used to punish misbehavior and toxicity. Previously you could only vote on the match's ECL page, with the ECL 2.0 Addon set-up however voting is just one click away. It also displays your current karma next to your name.
<br><br><br><br>


<div align="center">
    <img src="https://gitlab.com/ecl1/chrome-extension/raw/master/docs/flag.svg" alt="Flag icon"/>
    <h3>Reporting</h3>
</div>

ECL has set up their [own reporting tool][ecl-ticket-page] as the amount of reports we get would barely be manageable without it. To make reporting easier the ECL 2.0 Addon takes care of the nasty copy-and-paste-ing so you can focus on writing a good report.
<br><br><br><br>


<div align="center">
    <h2>Install</h2>
</div>

Once the Extension is on the Chrome Store and Mozilla Addons we publish the links here. For now only developers can install it.

After installation the addon is going to open your [ECL profile page][ecl-profile] where you have to authorize the addon by linking it with your account:

<div align="center">
    <img src="https://gitlab.com/ecl1/chrome-extension/raw/master/docs/linkacc.webp" alt="ECL profile link section"/>
</div>

If you forgot to do that the addon will remind you everytime you visit faceit and in it's popup:

<div align="center">
    <img src="https://gitlab.com/ecl1/chrome-extension/raw/master/docs/missinglink.webp" alt="Addon's popup"/>
</div>

<br>

<div align="center">
    <h2>Something's weird?</h2>
</div>


If you encounter any issues with it, write [#support][discord]. Thank you so much!

<br><br>
<hr/>
<br><br>

<div align="center">
    <h2>For Developers and testers</h2>
</div>

If you're interested on how the addon works have a peek inside, we assume you know your way around. The [DEVDOCS][dev-docs] might help you.
This project makes heavy use of [TypeScript][typescript], [Webpack][webpack] and the [WebExtensions API][webextensions-api].


If you like to help, [open an issue][new-issue].

<div align="center">
    <h3>Install pre-releases</h3>
</div>

Click on the corresponding badge above to download the latest release:
<div align="center">
    <a href="https://gitlab.com/ecl1/chrome-extension"><img src="https://gitlab.com/ecl1/chrome-extension/raw/master/docs/dlhere.jpg" alt="Where to download"/></a>
</div>

You can find all releases [here][tags]. Versions ending with `staging` use the `staging.ecl.gg` backend, with `beta` use `beta.ecl.gg` and `stable` the production backend. On the right side of each tag is a download button and you will find a __`Download Artifacts`__ section when you click on it. Here you can download the latest `build`. __Make sure not to download the source code if you don't intent to.__ If such an artifact is not available anymore you can still download the source code and [build it yourself][dev-docs].


#### Chromium based browsers

0. Unpack the artifact somewhere.
1. Go to `chrome://extensions/`
2. In the upper right corder click the `Developer mode` switch.
3. In the now visible bar click `Load unpacked extension`
4. Select the `<artifact>/dist/chrome` path. (Not the zip file!)
5. Profit

#### Firefox based browsers

0. Unpack the artifact somewhere.
1. Go to `about:debugging#/runtime/this-firefox`
2. Click on `Load temporary addon`
3. Navigate to `<artifact>/dist/ff.zip` (Not the directory!)
4. Profit

[typescript]: https://www.typescriptlang.org/
[webpack]: https://webpack.js.org/
[webextensions-api]: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions
[new-issue]: https://gitlab.com/ecl1/chrome-extension/issues/new?issue
[discord]: https://discordapp.com/invite/SGdtNEz
[tags]: https://gitlab.com/ecl1/chrome-extension/-/tags
[dev-docs]: https://gitlab.com/ecl1/chrome-extension/blob/master/docs/DEVDOCS.md#commands
[ecl-ticket-page]: https://ecl.gg/ticket
[ecl-profile]: https://ecl.gg/profile
