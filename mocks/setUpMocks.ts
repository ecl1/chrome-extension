import {MutationObserver} from './mutation-observer';

Object.defineProperty(window, 'MutationObserver', { value: MutationObserver, writable: true });

Object.defineProperty(window, 'ECLABRANCH', { value: 'staging', writable: true });
