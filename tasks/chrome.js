// Builds chrome addon
const run = require('./run');
const genManifest = require('./genManifest');

(async () => {
  const mode = process.argv[2]; // 'development' or 'production'
  await run(`npx webpack --config configs/webpack.config.js --mode ${mode} --env.browser='chrome'`);
  genManifest('chrome');
  await run('cd dist/chrome && zip -r ../chrome.zip . && cd ../../');
})().catch(e => console.log(e));
