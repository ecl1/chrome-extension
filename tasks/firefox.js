// Builds firefox addon
const run = require('./run');
const genManifest = require('./genManifest');
const path = require('path');

const OUT_DIR = path.join(__dirname, '../dist/ff');
const ART_DIR = path.join(OUT_DIR, './artifacts');

(async () => {
  const mode = process.argv[2]; // 'development' or 'production'
  await run(`npx webpack --config configs/webpack.config.js --mode ${mode} --env.browser='ff'`);
  genManifest('ff');
  await run('cd dist/ff && zip -r ../ff.zip . && cd ../../');
  if (mode !== 'production') {
    await run(`npx web-ext run -s ${OUT_DIR} -a ${ART_DIR}`);
  }
})().catch(e => {
  console.error(e);
  console.log('\n\tSTDOUT:\n', e.stdout);
  
});
