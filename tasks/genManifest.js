const path = require('path');
const fs = require('fs');

const MANIFEST_PATH = path.join(__dirname, '../src/manifest.json.js');
const DIST_PATH = path.join(__dirname, '../dist');

const packageJson = require('../package.json');

module.exports = (browser) => {
    const manifestFn = require(MANIFEST_PATH);
    const res = manifestFn(browser, packageJson);
    const outputPath = path.join(DIST_PATH, browser, 'manifest.json');
    fs.writeFileSync(outputPath, JSON.stringify(res, null, 4), 'utf-8');
}
