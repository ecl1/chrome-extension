# Changelog

## 1.6.1
 - Fixes issue that made beta and stable users use the staging backend

## 1.6.0
 - Adds git-hash to version
 - Adds Styles to CHANGELOG.md
 - Catches access-on-null-error
 - Karma buttons now don't hide but change opacity

## 1.5.0
- Adds Changelog
- Karma buttons now stay hidden
- Adds report buttons to players
- Adds Karma to user in navbar

## 1.4.0
- Fixes typo that caused troubles internally

## 1.3.0
- Improved error-handling

## 1.2.0
- Fixes bug that caused verbose errors to be thrown when a player's matches page was visited
- Adds better error-handling after a response
- Removes buttons now directly after interaction

## 1.1.1
- Version out of sync fixed

## 1.1.0
- Fixes bug that showed all karma arrows again after they were removed

## 1.0.0
- Initial release
- Integrates Karma System into Faceit matchrooms
