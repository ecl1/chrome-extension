const {execSync} = require('child_process');
const path = require("path");
const webpack = require('webpack');
const CopyPlugin = require("copy-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const SRC_DIR = path.join(__dirname, "../src");

module.exports = (env, argv) => ({
  entry: {
    faceit: path.join(SRC_DIR, "./cs/faceit.ts"),
    faceit_main_context: path.join(SRC_DIR, "./cs/mainContext/faceit_main_context.ts"),
    afterinstall: path.join(SRC_DIR, "./cs/afterinstall.ts"), // handles token request
    background: path.join(SRC_DIR, "./bg/index.ts"),
    popup: path.join(SRC_DIR, "./popup/index.ts"),
  },
  devtool: isProd(argv) ? undefined : "inline-source-map",
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: "ts-loader",
        exclude: /node_modules/,
        options: {
          configFile: path.join(__dirname, "./tsconfig.json"),
        },
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /CHANGELOG.md$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "changelog.html",
            }
          },
          {
            loader: "text-transform-loader",
            options: {
              prependText: '<!DOCTYPE html><html><head><link href="assets/changelog.css" rel="stylesheet"></head><body>',
              appendText: '</body></html>'
            }
          },
          "markdown-loader",
        ],
      }
    ],
  },
  resolve: {
    extensions: [".ts", ".js"],
  },
  output: {
    filename: "[name].js", // name: 'faceit' | 'background'
    path: path.resolve(__dirname, "../dist/" + getBrowser(env)),
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new CopyPlugin([
      {
        from: path.join(__dirname, "../resources/webextensions"),
        to: ".",
      }, // copy all files from resources/<browser> into root of dist
      ...(getBrowser(env) !== "ff" ? [{
        from: path.join(
          __dirname,
          "../node_modules/webextension-polyfill/dist/browser-polyfill.min.js",
        ),
      }] : []), // copy polyfill for browser.* promise-syntax
    ]),
    new webpack.DefinePlugin({
      ECLAVERSION: JSON.stringify(require(path.join(__dirname, '../package.json')).version),
      ECLABRANCH: JSON.stringify(require(path.join(__dirname, '../package.json')).branch),
      ECLASHA: JSON.stringify(getGitHash()),
    }),
    new HtmlWebpackPlugin({
      // popup.html
      template: path.join(SRC_DIR, "./popup/popup.html"),
      chunks: ["popup"],
      filename: "popup.html"
    }),
    new CleanWebpackPlugin(), // clean dist/<browser> before build
  ],
  optimization: {
    minimize: isProd(argv), // dont waste dev-time with minifier
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          ecma: undefined,
          warnings: false,
          parse: {},
          compress: {},
          output: {
            comments: false,
          },
          mangle: true, // Note `mangle.properties` is `false` by default.
          module: false,
          toplevel: false,
          nameCache: null,
          ie8: false,
          keep_classnames: undefined,
          keep_fnames: false,
          safari10: false,
        },
      }),
    ],
  },
});

function getBrowser(env) {
  if (!env.browser)
    throw new Error(
      `webpack.config.js [getBrowser()]: webpack must be started with '--env.browser='<browser>'' where <browser> is either 'ff' or 'chrome'`,
    );
  return env.browser;
}

function isProd(argv) {
  return argv.mode === "production";
}

function getGitHash() {
  const res = execSync("git rev-parse --short HEAD", { encoding: 'utf8' });
  console.log("git sha", res.trim());
  return res.trim();
}
