const path = require("path");
module.exports = {
    rootDir: path.join(__dirname, "../"),
    roots: ["<rootDir>/src/cs", "<rootDir>/src/bg", "<rootDir>/src/popup", "<rootDir>/src/shared"],
    transform: {
        "^.+\\.ts$": "ts-jest",
    },
    globals: {
        "ts-jest": {
            tsConfig: "<rootDir>/configs/tsconfig.json",
        },
    },
    automock: false,
    setupFiles: [
        path.join(__dirname, "../mocks/setUpMocks.ts"),
        path.join(__dirname, "../tasks/setupJest.ts"),
    ],
    collectCoverageFrom: [
        "src/**/*.ts"
    ]
};
